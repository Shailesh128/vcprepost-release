#---------------------------------------------------------------------------------------------------
## @file    preProcessorFlowVC.py
#  @author  Debanjan Mukherjee, University of California Berkeley
# 
# @brief This is a collection of functions that are used to pre-process particle transport simulations
# using the modified tracer integration code 'FlowVC'
#
# @note Usage: 
#   -To run this program as a script simply type python preProcessorFlowVC.py <arguments>
#   -Omitting the argument entry will print a usage function describing the arguments
#   -To run this program as a module for importing to another file, copy the file 'importer.py'
#   to the directory where your script is, and it should work fine
#
# List of functions:
#-------------------
# - createCoordinatesConnectivity
# - convertVTKDataToBin
# - convertSurfaceNormalsToBin
# - pointIn2DPolygon
# - getAngle2D
# - pointIn3DPolygon
# - getRodriguezRotationMatrix
# - getPolygonLocalCoordinates
# - getPolygon3DCoordinatesFromLocal
# - generateParticleInitialConfigOnPlane
# - generateParticleConfigurationUniformGrid
# - generateParticleInitialConfigFromMesh
# - generateMeshBounds
# - generateParticleInitialConfigFromVTKPlane
# - processFlowVCInputFileToDictionary
# - writeDictionaryToFlowVCInput
#
#---------------------------------------------------------------------------------------------------

#---------------------
# BEGIN MODULE IMPORTS
#---------------------
from __future__ import print_function

import sys, os

if sys.version_info.major < 3:
    input = input

try:
    import vtk
except ImportError:
    sys.exit("Could not import vtk in preProcessorFlowVC. Check vtk library configuration")

try:
    import numpy as np
except ImportError:
    sys.exit("Could not import other modules. Check python installation.")
#-------------------
# END MODULE IMPORTS
#-------------------

#-----------------------------------------------------------------------------------
## @brief A utility function that takes a VTK file as input, and extracts the data 
# from the file based on file type (e.g. polydata, unstructured etc.)
#
# @param[in] a_FileName Name of the VTK mesh/data file
# @param[in] a_Legacy   For legacy VTK file types, the data type (vtu/vtp) is needed
#
#-----------------------------------------------------------------------------------
def readVTK(a_FileName, a_Legacy='none'):

    #
    # read the input file based on extension
    #
    if a_FileName.endswith('.vtu'):
        reader = vtk.vtkXMLUnstructuredGridReader()
    elif a_FileName.endswith('.vtp'):
        reader = vtk.vtkXMLPolyDataReader()
    elif a_FileName.endswith('.vtk'):
        if a_Legacy == 'none':
            print("Need To Specify Data Type For Legacy Files")
            sys.exit()
        elif a_Legacy == 'vtu':
            reader = vtk.vtkUnstructuredGridReader()
        elif a_Legacy == 'vtp':
            reader = vtk.vtkPolyDataReader()
    else:
        print("Unsupported File Extension")
        sys.exit()
        
    print("Reading From File", a_FileName)

    reader.SetFileName(a_FileName)
    reader.Update()

    data = reader.GetOutput()

    return data

#---------------------------------------------------------------------------------------------------------
## @brief Function to take in a vtk mesh-data file and generate the mesh-data binary files in 
# FlowVC readable formats for coordinates, connectivity, and mesh topology
#
# @note Flow VC Mesh Data Format:
# 1. Coordinates File: Filename = <Prefix>_coordinates.bin
#    Coordinates data: npts x0 y0 z0 x1 y1 z1 ... x(N-1) y(N-1) z(N-1)
#
# 2. Connectivity File: Filename = <Prefix>_connectivity.bin
#    Connectivity data: ncells n00 n01 n02 n03 n10 n11 n12n 13 ... n(N-1)0 n(N-1)1 n(N-1)2 n(N-2)3
# 
# 3. Adjacency File: Filename = <Prefix>_adjacency.bin
#    Adjacency data: ncells c00 c01 c01 c02 c10 c11 c12 c13 c20 c21 c23 c24 ... 
#
# @param[in] a_FileName             Name of the vtk file to be converted to binary
# @param[in] a_OutFilePrefixRoot    Prefix for the velocity output name as described in documentation
# @param[in] a_InputLegacyDataType  If legacy vtk files are used, the data type vtu/vtp has to be specified
# @param[in] a_CreateAdjacency      Boolean flag for creating the adjacency/topology binary file
# @param[in] a_Offset               (to be replaced) Parameter to adjust spacing for binary data files
#
#----------------------------------------------------------------------------------------------------------
def createCoordinatesConnectivity(a_FileName, a_OutFilePrefixRoot, a_InputLegacyDataType = 'none', a_CreateAdjacency = True, a_Offset = 0):
    
    #
    # read the input file and extract the data
    #
    data    = readVTK(a_FileName, a_Legacy=a_InputLegacyDataType)
    pData   = data.GetPointData()

    numNodes    = data.GetNumberOfPoints()
    numCells    = data.GetNumberOfCells()

    #
    # create a dummy numpy array to just store the node number as an integer,
    # and an array of floats to store the coordinates of the nodes
    # 
    numNodesArrayToWrite    = numNodes*np.ones(1, dtype=np.int32)
    coordinatesArrayToWrite = np.zeros((numNodes, 3))

    #
    # populate the coordinates array from the vtk file
    #
    print("Converting Coordinates")

    progress = 0
    for p in range(numNodes):

        xyz = np.asarray(data.GetPoint(p))
        
        coordinatesArrayToWrite[p,:] = xyz
    
        if ((p * 100) / numNodes) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    #
    # open the file for coordinates in binary mode, and use the intrinsic 'tofile()'
    # method for numpy arrays to write the data into the file
    #
    coordsFileName  = a_OutFilePrefixRoot + '_coordinates.bin'
    coordsFileObj   = open(coordsFileName, 'wb')
    numNodesArrayToWrite.tofile(coordsFileObj)
    coordinatesArrayToWrite.tofile(coordsFileObj)
    coordsFileObj.close()

    print("\n Written Coordinates To", coordsFileName)

    #
    # create a dummy numpy array to just store the number of cells as an integer, 
    # and an array of integers to store the cell-nodal connectivity
    # NOTE: ALL ELEMENTS ARE ASSUMED TO BE TETRAHEDRALIZED (4 NODES PER CELL)
    #
    numCellsArrayToWrite      = numCells*np.ones(1, dtype=np.int32)
    connectivityArrayToWrite  = np.ones((numCells, 4), dtype=np.int32)

    #
    # populate the connectivity array from the vtk file, by iterating over cells
    #
    print("Converting Connectivity")

    progress = 0
    for c in range(numCells):
    
        cellNodeIDList = vtk.vtkIdList()

        cell = data.GetCell(c)

        if cell.GetNumberOfPoints() == 4:

            cellNodeList = cell.GetPointIds()
            
            for i in range(4):
                connectivityArrayToWrite[c, i] = cellNodeList.GetId(i) + a_Offset
    
        elif cell.GetNumberOfPoints() == 3:
      
            cellNodeList = cell.GetPointIds()
            for i in range(3):
                connectivityArrayToWrite[c, i] = cellNodeList.GetId(i) + a_Offset

            #connectivityArrayToWrite[c, 3] = 0
            connectivityArrayToWrite[c,3] = -1
        
        else:

            print("Non triangular and/or non tetrahedral elements not allowed")
            sys.exit()
    
        if ((c * 100) / numCells) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    #
    # open the file for connectivity in binary mode, and use the intrinsic 'tofile()'
    # method for numpy arrays to write the data into the file
    #
    connFileName  = a_OutFilePrefixRoot + '_connectivity.bin'
    connFileObj   = open(connFileName, 'wb')
    numCellsArrayToWrite.tofile(connFileObj)
    connectivityArrayToWrite.tofile(connFileObj)
    connFileObj.close()

    print("\n Written Connectivity To", connFileName)

    #
    # now we proceed to process the adjacency information
    #
    adjacencyArrayToWrite = -1*np.ones((numCells, 4), dtype = np.int32)
  
    nodesInCell             = vtk.vtkIdList()
    nodesInFaceOpposite1    = vtk.vtkIdList()
    nodesInFaceOpposite2    = vtk.vtkIdList()
    nodesInFaceOpposite3    = vtk.vtkIdList()
    nodesInFaceOpposite4    = vtk.vtkIdList()

    nodesInEdgeOpposite1    = vtk.vtkIdList()
    nodesInEdgeOpposite2    = vtk.vtkIdList()
    nodesInEdgeOpposite3    = vtk.vtkIdList()
    nodesInEdgeOpposite4    = vtk.vtkIdList()

    #
    # each cell assumed to have 4 nodes each
    #
    nodesInCell.SetNumberOfIds(4)

    #
    # each face of a cell thereof assumed to have 3 nodes each 
    # 
    nodesInFaceOpposite1.SetNumberOfIds(3)
    nodesInFaceOpposite2.SetNumberOfIds(3)
    nodesInFaceOpposite3.SetNumberOfIds(3)
    nodesInFaceOpposite4.SetNumberOfIds(3)

    #
    # each edge is assumed to have 2 nodes each
    #
    nodesInEdgeOpposite1.SetNumberOfIds(2)
    nodesInEdgeOpposite2.SetNumberOfIds(2)
    nodesInEdgeOpposite3.SetNumberOfIds(2)
    nodesInEdgeOpposite4.SetNumberOfIds(2)

    print("Resolving Adjacency")
    progress = 0

    for cell in range(numCells):
        
        data.GetCellPoints(cell, nodesInCell)
      
        if data.GetCellType(cell) == vtk.VTK_TETRA:
            cellAdjacency = np.zeros(4, dtype=np.int32)
        elif data.GetCellType(cell) == vtk.VTK_TRIANGLE:
            #cellAdjacency = -1*np.ones(4, dtype=np.int32)
            cellAdjacency = np.zeros(4, dtype=np.int32)
          
        cellID1 = vtk.vtkIdList()
        cellID2 = vtk.vtkIdList()
        cellID3 = vtk.vtkIdList()
        cellID4 = vtk.vtkIdList()

        #
        # NOTE: IN THE FOLLOWING LINES OF CODE, THE ORDERING OF NODES, AND FACES IS 
        # EXTREMELY CRUCIAL. THE FIRST THREE LINES ARE BASED ON MY OLDER CODE THAT
        # ASSUMES THAT FACE 1 IS THE FACE OPPOSITE NODE 1
        # THE SECOND THREE LINES ARE BASED ON THE MANUAL FROM SHAWN'S WEBSITE
        #
        
        if data.GetCellType(cell) == vtk.VTK_TETRA:
            
            nodesInFaceOpposite1.SetId(0, nodesInCell.GetId(0))
            nodesInFaceOpposite1.SetId(1, nodesInCell.GetId(2))
            nodesInFaceOpposite1.SetId(2, nodesInCell.GetId(3))
            
            nodesInFaceOpposite2.SetId(0, nodesInCell.GetId(0))
            nodesInFaceOpposite2.SetId(1, nodesInCell.GetId(1))
            nodesInFaceOpposite2.SetId(2, nodesInCell.GetId(3))
            
            nodesInFaceOpposite3.SetId(0, nodesInCell.GetId(0))
            nodesInFaceOpposite3.SetId(1, nodesInCell.GetId(1))
            nodesInFaceOpposite3.SetId(2, nodesInCell.GetId(2))
            
            nodesInFaceOpposite4.SetId(0, nodesInCell.GetId(1))
            nodesInFaceOpposite4.SetId(1, nodesInCell.GetId(2))
            nodesInFaceOpposite4.SetId(2, nodesInCell.GetId(3))

        elif data.GetCellType(cell) == vtk.VTK_TRIANGLE:

            nodesInEdgeOpposite1.SetId(0, nodesInCell.GetId(0))
            nodesInEdgeOpposite1.SetId(1, nodesInCell.GetId(2))

            nodesInEdgeOpposite2.SetId(0, nodesInCell.GetId(0))
            nodesInEdgeOpposite2.SetId(1, nodesInCell.GetId(1))

            nodesInEdgeOpposite3.SetId(0, nodesInCell.GetId(1))
            nodesInEdgeOpposite3.SetId(1, nodesInCell.GetId(2))

        #
        # find all cells using face (defined by node ID list) other than a specified cell
        # (this is a VTK Topological Inquiry function)
        #
        if data.GetCellType(cell) == vtk.VTK_TETRA:

            data.GetCellNeighbors(cell, nodesInFaceOpposite1, cellID1)
            data.GetCellNeighbors(cell, nodesInFaceOpposite2, cellID2)
            data.GetCellNeighbors(cell, nodesInFaceOpposite3, cellID3)
            data.GetCellNeighbors(cell, nodesInFaceOpposite4, cellID4)

        elif data.GetCellType(cell) == vtk.VTK_TRIANGLE:

            data.GetCellNeighbors(cell, nodesInEdgeOpposite1, cellID1)
            data.GetCellNeighbors(cell, nodesInEdgeOpposite2, cellID2)
            data.GetCellNeighbors(cell, nodesInEdgeOpposite3, cellID3)

        #
        # if a cell is internal, then the face should be topologically shared by two
        # cells, while if it is on boundary, some faces may be shared by only one 
        # using this idea, non boundary neighbors and boundary neighbors are distinguished
        #
        if data.GetCellType(cell) == vtk.VTK_TETRA:
    
            if cellID1.GetNumberOfIds() == 1:
                cellAdjacency[0] = cellID1.GetId(0)
            elif cellID1.GetNumberOfIds() == 0:
                cellAdjacency[0] = -1

            if cellID2.GetNumberOfIds() == 1:
                cellAdjacency[1] = cellID2.GetId(0)
            elif cellID2.GetNumberOfIds() == 0:
                cellAdjacency[1] = -1

            if cellID3.GetNumberOfIds() == 1:
                cellAdjacency[2] = cellID3.GetId(0)
            elif cellID3.GetNumberOfIds() == 0:
                cellAdjacency[2] = -1

            if cellID4.GetNumberOfIds() == 1:
                cellAdjacency[3] = cellID4.GetId(0)
            elif cellID4.GetNumberOfIds() == 0:
                cellAdjacency[3] = -1

        elif data.GetCellType(cell) == vtk.VTK_TRIANGLE:

            if cellID1.GetNumberOfIds() == 1: 
                cellAdjacency[0] = cellID1.GetId(0)
            elif cellID1.GetNumberOfIds() == 0:
                cellAdjacency[0] = -1

            if cellID2.GetNumberOfIds() == 1:
                cellAdjacency[1] = cellID2.GetId(0)
            elif cellID2.GetNumberOfIds() == 0:
                cellAdjacency[1] = -1

            if cellID3.GetNumberOfIds() == 1:
                cellAdjacency[2] = cellID3.GetId(0)
            elif cellID3.GetNumberOfIds() == 0:
                cellAdjacency[2] = -1
        
        #
        # update this cell adjacency array into the overall mesh adjacency array
        #
        adjacencyArrayToWrite[cell, :] = cellAdjacency

        if ((cell * 100) / numCells) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    #print ((adjacencyArrayToWrite))[:50, :]

    #
    # open the file for adjacency in binary mode, and use the intrinsic 'tofile()'
    # method for numpy arrays to write the data into the file
    #
    adjFileName  = a_OutFilePrefixRoot + '_adjacency.bin'
    adjFileObj   = open(adjFileName, 'wb')
    numCellsArrayToWrite.tofile(adjFileObj)
    adjacencyArrayToWrite.tofile(adjFileObj)
    adjFileObj.close()
    
    print("\n Written Adjacency To", adjFileName)

#----------------------------------------------------------------------------------------------------------
## @brief Function to convert the VTK velocity data files into binary format
#
# @note FlowVC mesh velocity data format:
# - velocity data output name: <Prefix>.TSTAMP.bin
# - velocity data format: ts u0 v0 w0 u1 v1 w1 u(N-1) v(N-1) w(N-1)
# 
# @note The utility of this file should be such that the is called from within a loop
# over multiple velocity time-step data sets for a time-varying field
#
# @param[in] a_FileName             Name of the vtk file to be converted to binary
# @param[in] a_OutFilePrefixRoot    Prefix for the velocity output name as described in documentation
# @param[in] a_OutFileTimeIndex     Time index for time-stamp in the output file name in documentation
# @param[in] a_OutFileTimeStamp     Actual time-stamp value to be written into the output binary file
# @param[in] a_DataFiledName (opt)  The name of the data filed that is written as the interpolant field
# @param[in] a_LegacyDataType (opt) If legacy vtk files are used, the data type vtu/vtp has to be specified
#
#----------------------------------------------------------------------------------------------------------
def convertVTKDataToBin(a_FileName, a_OutFilePrefixRoot, a_OutFileTimeIndex, a_OutFileTimeStamp, a_DataFieldName = 'velocity', a_LegacyDataType = 'none'):
    
    #
    # read the input file and extract the data
    #
    print("Reading From File: ", a_FileName)
    data        = readVTK(a_FileName, a_Legacy=a_LegacyDataType)
    numNodes    = data.GetNumberOfPoints()
    dataValues  = data.GetPointData().GetArray(a_DataFieldName)
    
    velocityDataArrayToWrite  = np.zeros((numNodes,3))
    timeStampArrayToWrite     = a_OutFileTimeStamp*np.ones(1)
    
    print("Extracting Data Fields")
    progress = 0

    for p in range(numNodes):

        vel = np.asarray(dataValues.GetTuple(p))
        velocityDataArrayToWrite[p,:] = vel

        if ((p * 100) / numNodes) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    #
    # open the file for velocity data in binary mode, and use the intrinsic 'tofile()'
    # method for numpy arrays to write the data into the file
    #
    dataFileName  = a_OutFilePrefixRoot +'_vel.' + str(a_OutFileTimeIndex) + '.bin'
    dataFileObj   = open(dataFileName, 'wb')
    timeStampArrayToWrite.tofile(dataFileObj)
    velocityDataArrayToWrite.tofile(dataFileObj)
    dataFileObj.close()

    print("\n Written Binary Data To", dataFileName)

#------------------------------------------------------------------------------------------------------------------------
## @brief Function to convert a VTU data file into a file that describes the normals of the walls (no-slip boundaries)
# in binary format for FlowVC to read (needed for contact mechanics of particles)
#
# @param[in] a_FileName             A VTU Data file, preferably where velocities are prominently developed (e.g. systole)
# @param[in] a_OutFilePrefixRoot    Root string that forms the basis of the output file prefixes
# @param[in] a_VariableName         Name of the data that defines the no-slip (usually 'velocity' or 'vector_field')
# @param[in] a_ReverseNormals       Choice variable to decide whether the normal vectors would need to be flipped
# @param[in] a_LegacyDataType       For lgecay vtk input files, the data type has to be explicitly specified
#
#------------------------------------------------------------------------------------------------------------------------
def convertSurfaceNormalsToBin(a_FileName, a_OutFilePrefixRoot, a_VariableName = 'velocity', a_ReverseNormals = False, a_LegacyDataType = 'vtu'):

    TINY = 1.0e-12

    #
    # read the input file and extract the data
    #
    data        = readVTK(a_FileName, a_Legacy=a_LegacyDataType)
    numNodes    = data.GetNumberOfPoints()

    #
    # create a new data field "NodeID" that saves integer index number for each node
    #
    print("Creating Node Indices")
    progress = 0

    nodeIndex   = vtk.vtkIntArray()
    nodeIndex.SetNumberOfValues(numNodes)
    nodeIndex.SetName('NodeID')

    for i in range(numNodes):
        nodeIndex.SetValue(i,i)
        if ((i * 100) / numNodes) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    data.GetPointData().SetScalars(nodeIndex)
    
    #
    # extract the exterior surface of the overall model
    #
    surface = vtk.vtkDataSetSurfaceFilter()
    surface.SetInputData(data)
    surface.Update()

    #
    # get the normal at each surface node
    #
    normals = vtk.vtkPolyDataNormals()
    normals.SetInputConnection(surface.GetOutputPort())
    normals.SetFeatureAngle(60)
    normals.Update()

    #
    # save the number of nodes in a surface mesh
    #
    numSurfaceNodes = normals.GetOutput().GetNumberOfPoints()

    #
    # extract surface nodes that have (effectively) zero velocity 
    #
    print("\n Extracting No-slip Nodes")
    progress = 0

    noSlipNodeFlag  = np.zeros(numSurfaceNodes, dtype=np.int32)
    noSlipNodeCount = 0

    assert(normals.GetOutput().GetPointData().SetActiveAttribute(a_VariableName, 1) != -1)
    
    for p in range(numSurfaceNodes):
        U = np.zeros(3)
        normals.GetOutput().GetPointData().GetAttribute(1).GetTuple(p,U)

        if np.linalg.norm(np.asarray(U)) <= TINY:
            noSlipNodeFlag[p]   = 1
            noSlipNodeCount     = noSlipNodeCount + 1

        if ((p * 100) / numSurfaceNodes) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    print("Number of no-slip boundary nodes", noSlipNodeCount)

    noSlipNodeCountArrayToWrite = noSlipNodeCount*np.ones(1, dtype=np.int32)

    #
    # construct 1-d array to hold NodeID's of surface nodes with (near) zero velocity
    #
    print("\n Extracting The Surface Node ID's (With No Slip Velocity)")
    progress = 0

    noSlipNodes = np.zeros(noSlipNodeCount, dtype = np.int32)
    noSlipCount = 0

    assert(normals.GetOutput().GetPointData().SetActiveAttribute("NodeID", 0) != -1)

    for p in range(numSurfaceNodes):
        
        if noSlipNodeFlag[p] == 1:

            nID = np.zeros(1, dtype = np.int32)

            assert(noSlipCount < noSlipNodeCount)
            normals.GetOutput().GetPointData().GetAttribute(0).GetTuple(p,nID)
            noSlipNodes[noSlipCount]    = int(nID[0])
            noSlipCount                 = noSlipCount + 1
        
        if ((p * 100) / numNodes) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    #
    # construct a 2-d array to hold NodeID's of surface nodes with (near) zero velocity
    #
    print("\n Extracting The Surface Normals (With No Slip Velocity")
    progress = 0

    nodeNormals = np.zeros((noSlipNodeCount, 3))

    assert(normals.GetOutput().GetPointData().SetActiveAttribute("Normals",2) != -1)
    noSlipCount = 0

    for i in range(numSurfaceNodes):

        if noSlipNodeFlag[i] == 1:

            nVec = np.zeros(3)

            assert(noSlipCount < noSlipNodeCount)
            normals.GetOutput().GetPointData().GetAttribute(2).GetTuple(i, nVec)
            nodeNormals[noSlipCount,:] = np.asarray(nVec)
            noSlipCount = noSlipCount + 1
        
        if ((i * 100) / numNodes) > progress:
            print(progress,)
            sys.stdout.flush()
            progress += 5

    #
    # flip the normal directions if desired
    #
    if a_ReverseNormals:
        print("\n Flipping Normal Vector Directions")
        nodeNormals = -nodeNormals

    #
    # open and write the otput binary file
    #
    nodeFileName    = a_OutFilePrefixRoot+'_normals.bin'
    nodeFileObj     = open(nodeFileName, 'wb')
    noSlipNodeCountArrayToWrite.tofile(nodeFileObj)
    noSlipNodes.tofile(nodeFileObj)
    nodeNormals.tofile(nodeFileObj)
    nodeFileObj.close()
    
    print("Written Binary Data To", nodeFileName)

#-------------------------------------------------------------------------------------------------------
## @brief Python wrapper function for creating boundary flags using the bd_elements.c code that 
#  determines elements containing nodes with zero velocity.
#
#  @note Note the following:
#  - Code uses data from $fileprefix_vel.$TS.bin, $fileprefix_coordinates.bin 
#  and $fileprefix_connectivity.bin.
#  - Contents of output file: number of elements (int), element flags 
#  (int: 0 if interior element, 1 if element on boundary).
#  - Output file has name fileprefix_bflags.bin naming convention for use with flowVC.
#
# @param[in] a_InputPrefix  File prefix for the binary data file (see descrption)
# @param[in] a_TimeIndex    Time index for the binary data file (see description)
# @param[in] a_Dimensions   The geometry dimensions (3/2/1 etc)
# @param[in] a_BDFlagsPath  The optional argument to specify the path to the application 
# bdflags.out being called by the wrapper
#
#------------------------------------------------------------------------------------------------------
def createBoundaryFlagBinFile(a_InputPrefix, a_TimeIndex, a_Dimensions, a_BDFlagsPath=None):
    
    try:
        import subprocess as sp
    except ImportError:
        print("Could Not Import Module subprocess")
        sys.exit()

    if a_BDFlagsPath is None:
        pCommList   = ["./bd_elements.out", str(a_Dimensions), a_InputPrefix, str(a_TimeIndex)]
    else:
        pCommList   = [a_BDFlagsPath+"bd_elements.out", str(a_Dimensions), a_InputPrefix, str(a_TimeIndex)]

    sp.call(pCommList)

#--------------------------------------------------------------------------------
## @brief Python function to detect whether a point (a_Point) lies inside a 2D polygon
#
# @param[in] a_N            Number of vertices
# @param[in] a_PolyVertices numvertices X 3 coordinates (with 3rd coordinate = 0)
# @param[in[ a_Point        The coordinates of the point to be checked for
#
#--------------------------------------------------------------------------------
def pointIn2DPolygon(a_PolyVertices, a_N, a_Point):
    
    angle = 0
    
    for i in range(a_N):
        
        p1X = a_PolyVertices[i,0] - a_Point[0]
        p1Y = a_PolyVertices[i,1] - a_Point[1]
        p2X = a_PolyVertices[np.mod(i+1, a_N), 0] - a_Point[0]
        p2Y = a_PolyVertices[np.mod(i+1, a_N), 1] - a_Point[1]

        angle = angle + getAngle2D(p1X, p1Y, p2X, p2Y)
  
    if abs(angle) < np.pi:
        return False
    else:
        return True

#-----------------------------------------------------------------------------------------------
## @brief Python function to get the angle subtended by a line segment to the centroid of the 2D polygon
# 
# @param[in] a_X1 Planar X coordinate of start point of line segment
# @param[in] a_Y1 Planar Y coordinate of start point of line segment
# @param[in] a_X2 Planar X coordinate of end point of line segment
# @param[in] a_Y2 Planar Y coordinate of end point of line segment
#
#-----------------------------------------------------------------------------------------------
def getAngle2D(a_X1, a_Y1, a_X2, a_Y2):
    
    theta1 = np.arctan2(a_Y1, a_X1)
    theta2 = np.arctan2(a_Y2, a_X2)
    dtheta = theta2 - theta1
    
    while dtheta > np.pi:
        dtheta = dtheta - 2.0*np.pi
        
    while dtheta < - np.pi:
        dtheta = dtheta + 2.0*np.pi

    return dtheta

#--------------------------------------------------------------------------------
## @brief Python function to detect whether a point (a_Point) lies inside a 3D polygon
#
# @param[in] a_N Number of vertices
# @param[in] a_PolyVertices numvertices X 3 coordinates (with 3rd coordinate = 0)
# @param[in[ a_Point The coordinates of the point to be checked for
#
#--------------------------------------------------------------------------------
def pointIn3DPolygon(a_PolyVertices, a_N, a_Point):

    EPSILON     = 1.0e-12
    angleSum    = 0.0

    for i in range(a_N):

        p1X = a_PolyVertices[i,0] - a_Point[0]
        p1Y = a_PolyVertices[i,1] - a_Point[1]
        p1Z = a_PolyVertices[i,2] - a_Point[2]
        p2X = a_PolyVertices[np.mod(i+1, a_N), 0] - a_Point[0]
        p2Y = a_PolyVertices[np.mod(i+1, a_N), 1] - a_Point[1]
        p2Z = a_PolyVertices[np.mod(i+1, a_N), 2] - a_Point[2]

        m1 = np.sqrt(p1X*p1X + p1Y*p1Y + p1Z*p1Z)
        m2 = np.sqrt(p2X*p2X + p2Y*p2Y + p2Z*p2Z)

        if ( m1*m2 <= EPSILON ):
            angleSum = 2.0*np.pi
            break
        else:
            cosTheta = (p1X*p2X + p1Y*p2Y + p1Z*p2Z)/(m1*m2)

        angleSum = angleSum + np.arccos(cosTheta)
    
    if abs(angleSum - 2.0*np.pi) <= EPSILON:
        return True
    else:
        return False

#-------------------------------------------------------------------------------------
## @brief Get the rotation transformation matrix based on a Rodriguez rotation 
# parametrization. This esentially rotates Vector 0 to Vector R
#
# @param[in] a_Vector0 The vector which you want to roate
# @param[in] a_VectorR The vector that is to be rotated to
#
#-------------------------------------------------------------------------------------
def getRodriguezRotationMatrix(a_Vector0, a_VectorR):

    if ( type(a_Vector0) is list and type(a_VectorR) is list ) or ( type(a_Vector0) is tuple and type(a_VectorR) is tuple ):
        V0 = np.asarray(a_Vector0)
        VR = np.asarray(a_VectorR)
    elif ( type(a_Vector0) is np.ndarray ) and ( type(a_VectorR) is np.ndarray ):
        V0 = a_Vector0
        VR = a_VectorR
    else:
        print("Arguments To getRodriguezRotationMatrix are either or both wrong type")
  
    magV0 = np.linalg.norm(V0)
    magVR = np.linalg.norm(VR)

    if magV0 == 0.0 or magVR == 0.0:
        print("Zero Magnitude Vectors Cannot be Rotated")
        sys.exit()

    if not(magV0 == 1.0):
        V0 = V0/magV0

    if not(magVR == 1.0):
        VR = VR/magVR

    V0dotVR     = np.dot(V0, VR)
    V0crossVR   = np.cross(V0, VR)

    theta       = np.arccos(V0dotVR/(magV0*magVR))

    VA          = V0crossVR/np.linalg.norm(V0crossVR)

    skewA       = np.zeros((3,3))

    skewA[0,:]  = np.array([0.0, -VA[2], VA[1]])
    skewA[1,:]  = np.array([VA[2], 0.0, -VA[0]])
    skewA[2,:]  = np.array([-VA[1], VA[0], 0.0])

    ident       = np.identity(3)
    skewA2      = np.linalg.matrix_power(skewA,2)

    R           = ident + np.sin(theta)*skewA + (1.0 - np.cos(theta))*skewA2

    err1        = np.linalg.norm(np.dot(R, a_Vector0) - a_VectorR)
    err2        = abs(np.linalg.det(R) - 1.0)

    if  err1 > 1.0e-12:
        print("Warning: Rotation accuracy is at", err1)

    if err2 > 1.0e-12:
        print("Warning: Rotation determinant differs from unity by", err2)

    R = np.real_if_close(R, tol=1000)

    return R

#----------------------------------------------------------------------------
## BUGGY
#----------------------------------------------------------------------------
def getPolygonLocalCoordinates(a_PolyVertices, a_N):

    p0      = a_PolyVertices[0,:]
    p1      = a_PolyVertices[10,:]
    p2      = a_PolyVertices[20,:]

    loc0    = p0
    locx    = p1 - loc0
    normal  = np.cross(locx, p2 - loc0)
    locy    = np.cross(normal, locx)

    locx    = locx/np.linalg.norm(locx)
    locy    = locy/np.linalg.norm(locy)

    localCoordinates = np.zeros((a_N,2))

    for p in range(a_N):

        point   = a_PolyVertices[p,:]
        point2D = np.array([np.dot(point-loc0, locx), np.dot(point-loc0, locy)])
        localCoordinates[p,:] = point2D

    return localCoordinates

#--------------------------------------------------------------------------------
## BUGGY
#--------------------------------------------------------------------------------
def getPolygon3DCoordinatesFromLocal(a_PolyVertices, a_N, a_Local):

    p0      = a_PolyVertices[0,:]
    p1      = a_PolyVertices[1,:]
    p2      = a_PolyVertices[2,:]

    loc0    = p0
    locx    = p1 - loc0
    normal  = np.cross(locx, p2 - loc0)
    locy    = np.cross(normal, locx)

    locx    = locx/np.linalg.norm(locx)
    locy    = locy/np.linalg.norm(locy)

    p3D     = loc0 + a_Local[0]*locx + a_Local[1]*locy

    return p3D

#--------------------------------------------------------------------------------------------------------------------
## @brief Generate a random sample of non-overlapping points that can be placed within a polygon with a 
# specified set of vertices. These points are assumed to be seeded uniformly randomly, without any padding or sizing. 
#
# @param[in] a_PolyVertices A numpoints X 3 numpy array of the coordinates of the polygonal surface
# @param[in] a_NumPoints    The number of points to be generated on the surface
# @param[in] a_Dim          The number of space dimensions
#
#--------------------------------------------------------------------------------------------------------------------
def generateParticleInitialConfigOnPlane(a_PolyVertices, a_NumPoints, a_Padding, a_Dim=3):
    
    try:
        import random
    except ImportError:
        print("Could Not Import Module random For generateParticleInitialConfigOnPlane")
    
    useRotation = True
    useLocalXYZ = False

    #
    # point coordinates array declared as an  empty array
    #
    coordinates   = np.zeros((a_NumPoints, a_Dim))

    #
    # number of polygonal boundary vertices
    #
    numVertices   = a_PolyVertices.shape[0]
    
    if useRotation:
        #
        # rotate the plane such that it now lies on the x-y plane with positive z-normal
        #
        planeNormal   = np.cross(a_PolyVertices[1,:] - a_PolyVertices[0,:], a_PolyVertices[2,:] - a_PolyVertices[0,:])
        planeNormal   = planeNormal/np.linalg.norm(planeNormal)
        targetNormal  = np.array([0.0, 0.0, 1.0])
        rotation      = getRodriguezRotationMatrix(planeNormal, targetNormal)

        transformedVertices = np.zeros_like(a_PolyVertices)

        for p in range(numVertices):

            transformedVertices[p,:] = np.dot(rotation, a_PolyVertices[p,:])

        #
        # get the bounding box of this transformed plane, to sample points uniformly in the box
        #
        xBoundBoxMax  = np.max(transformedVertices[:,0])
        xBoundBoxMin  = np.min(transformedVertices[:,0])
        yBoundBoxMax  = np.max(transformedVertices[:,1])
        yBoundBoxMin  = np.min(transformedVertices[:,1])
        zBoundBoxMax  = np.max(transformedVertices[:,2])
        zBoundBoxMin  = np.min(transformedVertices[:,2])

        print(xBoundBoxMin, xBoundBoxMin)
        print(yBoundBoxMin, yBoundBoxMax)
        print(zBoundBoxMin, zBoundBoxMax)

    elif useLocalXYZ:

        transformedVertices = np.zeros((numVertices,2))

        transformedVertices = getPolygonLocalCoordinates(a_PolyVertices, numVertices)

        #
        # get the bounding box of this transformed plane, to sample points uniformly in the box
        #
        xBoundBoxMax  = np.max(transformedVertices[:,0])
        xBoundBoxMin  = np.min(transformedVertices[:,0])
        yBoundBoxMax  = np.max(transformedVertices[:,1])
        yBoundBoxMin  = np.min(transformedVertices[:,1])

        print(xBoundBoxMin, xBoundBoxMin)
        print(yBoundBoxMin, yBoundBoxMax)


    isTerminate   = False
    numGenerated  = 0

    #
    # keep generating points until termination criteria is not satisfied
    # for this sampling, termination criteria is true when all required points have been generated
    #
    while not(isTerminate):
        
        if useRotation:

            x = random.uniform(xBoundBoxMin, xBoundBoxMax)
            y = random.uniform(yBoundBoxMin, yBoundBoxMax)
            z = random.uniform(zBoundBoxMin, zBoundBoxMax)

            point = np.array([x, y, z])


            if pointIn2DPolygon(transformedVertices, numVertices, point):

                if numGenerated == 0:
        
                    samplePoints      = np.zeros((1,3))
                    samplePoints[0,:] = point
                    numGenerated      = numGenerated + 1

                elif numGenerated > 0:

                    numOverLaps = 0

                    for p in range(numGenerated):

                        #print np.linalg.norm(point - samplePoints[p,:]) , a_Padding

                        if np.linalg.norm(point - samplePoints[p,:]) <= a_Padding:

                            numOverLaps = numOverLaps + 1

                    if numOverLaps == 0:

                        samplePoints = np.vstack((samplePoints, point))
                        numGenerated = numGenerated + 1

            print("Generated Point", numGenerated)
            sys.stdout.flush()

        elif useLocalXYZ:

            x       = random.uniform(xBoundBoxMin, xBoundBoxMax)
            y       = random.uniform(yBoundBoxMin, yBoundBoxMax)
            point   = np.array([x, y])
            
            if pointIn2DPolygon(transformedVertices, numVertices, point):

                print("Point is in 2D Polygon")

                if numGenerated == 0:

                    samplePoints        = np.zeros((1,2))
                    samplePoints[0,:]   = point
                    numGenerated        = numGenerated + 1

                elif numGenerated > 0:

                    numOverLaps = 0

                    for p in range(numGenerated):

                        if np.linalg.norm(point - samplePoints[p,:]) <= a_Padding:

                            numOverLaps = numOverLaps + 1

                    if numOverLaps == 0:

                        samplePoints = np.vstack((samplePoints, point))
                        numGenerated = numGenerated + 1
            
            print("Generated Point", numGenerated)
            sys.stdout.flush()

        isTerminate = (numGenerated >= a_NumPoints)

    if useRotation:
        for p in range(a_NumPoints):
            samplePoints[p,:] = np.dot(np.linalg.inv(rotation), samplePoints[p,:])

        return samplePoints

    elif useLocalXYZ:
        samplePoints3D = np.zeros((a_NumPoints,3))
        for p in range(a_NumPoints):
            samplePoints3D[p,:] = getPolygon3DCoordinatesFromLocal(a_PolyVertices, numVertices, samplePoints[p,:])

        return samplePoints3D

#-----------------------------------------------------------------------------------------------
## @brief For a given set of polynomial vertices, a uniform grid encompassing the bounding box of the
# vertices is generated, and particles are seeded in a uniform grid within that bounding box
#
# @param[in] a_PolyVertices numPoints X 3 array of polygon vertices 
# @param[in] a_GridSpacing  spacing for the uniform grid along each axis (constant for all axis)
#
#-----------------------------------------------------------------------------------------------
def generateParticleConfigurationUniformGrid(a_PolyVertices, a_GridSpacing):

    #
    # number of polygonal boundary vertices
    #
    numVertices         = a_PolyVertices.shape[0]

    transformedVertices = np.zeros((numVertices,2))
    transformedVertices = getPolygonLocalCoordinates(a_PolyVertices, numVertices)

    #
    # get the bounding box of this transformed plane, to sample points uniformly in the box
    #
    xBoundBoxMax  = np.max(transformedVertices[:,0])
    xBoundBoxMin  = np.min(transformedVertices[:,0])
    yBoundBoxMax  = np.max(transformedVertices[:,1])
    yBoundBoxMin  = np.min(transformedVertices[:,1])

    print(xBoundBoxMin, xBoundBoxMin)
    print(yBoundBoxMin, yBoundBoxMax)

    numPointsX = int((xBoundBoxMax - xBoundBoxMin)/a_GridSpacing)+1
    numPointsY = numPointsX

    #
    # point coordinates array declared as an  empty array
    #
    coordinates     = np.zeros(((numPointsX)*(numPointsY), 3))

    pCount          = 0

    for px in range(numPointsX):
        for py in range(numPointsY):

            x       = xBoundBoxMin + float(px)*a_GridSpacing
            y       = yBoundBoxMin + float(py)*a_GridSpacing
            point2D = np.array([x,y])
            point3D = getPolygon3DCoordinatesFromLocal(a_PolyVertices, numVertices, point2D)
            
            coordinates[pCount, :] = point3D
            
            pCount = pCount + 1

    return coordinates

#----------------------------------------------------------------------------------------------------------
## @brief Generate a particle initial configuration based on a polydata mesh of triangular elements, such that
# a particle is placed at the centroid of each triangle.
#
# @param[in] a_FileName             VTK File defining the triangle mesh
# @param[in] a_OutFileName          The output file, which is a polydata file with all particle locations
# @param[in] a_TranslateRadius      Translate the centers of generated particles by one radius along normal
# @param[in] a_InputLegacyDataType  For lgecay vtk input files, the data type has to be explicitly specified
#
#-----------------------------------------------------------------------------------------------------------
def generateParticleInitialConfigFromMesh(a_FileName, a_OutFileName, a_TranslateRadius = 0.0, a_InputLegacyDataType = 'none'):

    #
    # read the input file and extract the data
    #
    data = readVTK(a_FileName, a_Legacy=a_InputLegacyDataType)

    numPoints = data.GetNumberOfPoints()
    numCells  = data.GetNumberOfCells()

    releasePoints = vtk.vtkPoints()
    releasePoints.SetNumberOfPoints(numCells)

    for c in range(numCells):

        cell = data.GetCell(c)
        
        if (cell.GetNumberOfPoints() != 3):
            print('Only Triangulated Surface Meshes Can Be Used For This. Aborting')
            sys.exit()
        else:
            pts = vtk.vtkPoints()
            pts = cell.GetPoints()
            p1  = pts.GetPoint(0)
            p2  = pts.GetPoint(1)
            p3  = pts.GetPoint(2)
            c0  = (np.asarray(p1) + np.asarray(p2) + np.asarray(p3))/3.0
            
            if a_TranslateRadius != 0.0:
                
                normal = np.zeros(3)
                cell.ComputeNormal(p1, p2, p3, normal)
                norMag  = np.sqrt(normal[0]**2 + normal[1]**2 + normal[2]**2)

                if norMag != 1.0:
                    normal[0] = normal[0]/norMag
                    normal[1] = normal[1]/norMag
                    normal[2] = normal[2]/norMag

                c0      = c0 + a_TranslateRadius*np.asarray(normal)

            releasePoints.SetPoint(c, c0)

    initConfig = vtk.vtkPolyData()
    initConfig.SetPoints(releasePoints)

    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(a_OutFileName)

    if vtk.VTK_MAJOR_VERSION <=5.0:
        writer.SetInput(initConfig)
    else:
        writer.SetInputData(initConfig)

    writer.Write()

#-----------------------------------------------------------------------------------------------------
## @brief Generate a particle init config file from another particle init config file, by choosing 
# a selected sub-set of particles from the second file
#
# @param[in] a_FileIn   Filename for the original particle init config file
# @param[in] a_FileOut  Filename with reduced number of particles
# @param[in] a_EveryNPoints Enter N, such that every N'th point is retained, set None to remove this
# not operate in this mode
# @param[in] a_FirstNPoints Enter N, such that the first N points are retained, set None to not operate 
# using this mode
# @param[in] a_LastNPoints Enter N, such that the last N points are retained, set None to not operate 
# using this mode
#
#-----------------------------------------------------------------------------------------------------
def modifyParticleInitConfig(a_FileIn, a_FileOut, a_EveryNPoints=None, a_FirstNPoints=None, a_LastNPoints=None):
    
    #
    # read the polydata file containign the particles
    #
    if a_FileIn.endswith('.vtk'):
        reader = vtkPolyDataReader()
    elif a_FileIn.endswith('.vtp'):
        reader = vtkXMLPolyDataReader()
    else:
        print("Unsupported File Extension For Argument a_FileIn")
        sys.exit()

    if not(a_FileOut.endswith('vtk') or a_FileOut.endswith('vtp')):
        print("Unsupported File Extension For Argument a_FileOut")
        sys.exit()
    
    if a_EveryNPoints is not None:

        reader.SetFileName(a_FileIn)
        reader.Update()

        data = reader.GetOutput()

        numPoints = data.GetNumberOfPoints()

        idList      = range(numPoints)
        idSelect    = idList[::a_EveryNPoints]

        reducedPoints   = vtkPoints()
        reducedPoints.SetNumberOfPoints(len(idSelect))
        counter         = 0

        for id in idSelect:

            xyzP = data.GetPoint(id)
            reducedPoints.SetPoint(counter, xyzP[0], xyzP[1], xyzP[2])
            counter = counter + 1

        reducedData = vtkPolyData()
        reducedData.SetPoints(reducedPoints)

        writer = vtkPolyDataWriter()
        writer.SetFileName(a_FileOut)
        
        if VTK_MAJOR_VERSION <=5.0:
            writer.SetInput(reducedData)
        else:
            writer.SetInputData(reducedData)

        writer.Update()
        writer.Write()

    if a_FirstNPoints is not None:
        print("This feature not implemented yet")
        sys.exit()

    if a_LastNPoints is not None:
        print("This feature not implemented yet")
        sys.exit()

#-----------------------------------------------------------------------------------------------------
## @brief Generate the bounding boxes for a VTU data set for use in FlowVC as input
#
# @param[in] a_FileName         Filename for the VTU file wich is used for the FloVC simulation
# @param[in] a_BoundBoxPadding  (optional) Extra padding space to be added to the bounding box extents
# @param[out] bounds            List of bounds as [xmin, xmax, ymin, ymax, zmin, zmax]
#
#-----------------------------------------------------------------------------------------------------
def generateMeshBounds(a_FileName, a_LegacyDataType = 'none', a_BoundBoxPadding=None):

  #if a_FileName.endswith('.vtu'):
  #  reader = vtk.vtkXMLUnstructuredGridReader()
  #elif a_FileName.endswith('.vtk'):
  #  reader = vtk.vtkUnstructuredGridReader()
  #else:
  #  print("Unsupported File Extension")
  #  sys.exit()

  #reader.SetFileName(a_FileName)
  #reader.Update()

  data = readVTK(a_FileName, a_Legacy=a_LegacyDataType)

  a_Bounds = np.asarray(data.GetBounds())

  if a_BoundBoxPadding != None:

    if type(a_BoundBoxPadding) is float:
      a_Bounds[0] = a_Bounds[0] - a_BoundBoxPadding
      a_Bounds[1] = a_Bounds[1] + a_BoundBoxPadding
      a_Bounds[2] = a_Bounds[2] - a_BoundBoxPadding
      a_Bounds[3] = a_Bounds[3] + a_BoundBoxPadding
      a_Bounds[4] = a_Bounds[4] - a_BoundBoxPadding
      a_Bounds[5] = a_Bounds[5] + a_BoundBoxPadding

    elif type(a_BoundBoxPadding) is list and len(a_BoundBoxPadding) == 3:
      a_Bounds[0] = a_Bounds[0] - a_BoundBoxPadding[0]
      a_Bounds[1] = a_Bounds[1] + a_BoundBoxPadding[0]

      a_Bounds[2] = a_Bounds[2] - a_BoundBoxPadding[1]
      a_Bounds[3] = a_Bounds[3] + a_BoundBoxPadding[1]

      a_Bounds[4] = a_Bounds[4] - a_BoundBoxPadding[2]
      a_Bounds[5] = a_Bounds[5] + a_BoundBoxPadding[2]

    elif type(a_BoundBoxPadding) is list and len(a_BoundBoxPadding) == 6:
      a_Bounds[:] = a_Bounds[:] + a_BoundBoxPadding[:]

    else:
      print("Incorrectly specified argument a_BoundBoxPadding")
      sys.exit()

  return a_Bounds

#----------------------------------------------------------------------------------------------------
## @brief Using the appropriate particle configuration generaion algorithm, this function will
# generate the particle configurations from a VTK input file, and save it in either 
# a vtk polydata (legacy format), or ascii data file. 
#
# @note The main algorithm behind this is the vtkFeatureEdges algorithm.
#
# @param[in] a_VTKInputFile     Input file of the polydata surface on which particles are seeded
# @param[in] a_NumPoints        Number of points required to be seeded
# @param[in] a_LegacyDataType   If the file is in legacy VTK format, you have to specify the data type
# @param[in] a_OutFileName      The name of the outputfile to which data is to be written
#
#-----------------------------------------------------------------------------------------------------
def generateParticleInitialConfigFromVTKPlane(a_VTKInputFile, a_NumPoints, a_OutFileName, a_InputLegacyDataType = 'none'):

  #
  # create a reader based on the file name and file type
  #
  if a_VTKInputFile.endswith('.vtp'):
    reader = vtkXMLPolyDataReader()
  elif a_VTKInputFile.endswith('.vtk'):
    if a_InputLegacyDataType == 'none':
      print("Need To Specify Data Type For Legacy Files")
      sys.exit()
    elif a_InputLegacyDataType == 'vtp':
      reader = vtkPolyDataReader()
  else:
    print("Unsupported File Extension")
    sys.exit()

  reader.SetFileName(a_VTKInputFile)
  reader.Update()

  data = reader.GetOutput()

  numPoints = data.GetNumberOfPoints()
  numCells  = data.GetNumberOfCells()

  #
  # extract the boundary points of the polydata surface
  #
  feFilter = vtkFeatureEdges()
  feFilter.SetInput(data)

  feFilter.BoundaryEdgesOn()
  feFilter.FeatureEdgesOff()
  feFilter.NonManifoldEdgesOff()
  feFilter.ManifoldEdgesOff()

  feFilter.Update()

  boundaryData      = vtkPolyData()
  boundaryData      = feFilter.GetOutput()

  boundaryWriter    = vtkPolyDataWriter()
  boundaryWriter.SetInput(boundaryData)
  boundaryWriter.SetFileName('boundary.vtk')
  boundaryWriter.Write()

  numBoundaryPoints = boundaryData.GetNumberOfPoints()
  boundaryVertices  = np.zeros((numBoundaryPoints, 3))

  for p in range(numBoundaryPoints):

    xyz = boundaryData.GetPoint(p)
    boundaryVertices[p,:] = np.asarray(xyz)

  #
  #
  #
  print("Generating Sample Points")
  numPoints     = a_NumPoints
  samplePoints  = generateParticleInitialConfigOnPlane(boundaryVertices, numPoints, 0.1)
  #samplePoints = generateParticleConfigurationUniformGrid(boundaryVertices, 1.0)
  #numPoints = samplePoints.shape[0]

  if a_OutFileName.endswith('vtk') or a_OutFileName.endswith('vtp'):

    outData   = vtkPolyData()
    outPoints = vtkPoints()

    outPoints.SetNumberOfPoints(a_NumPoints)
    #outPoints.SetNumberOfPoints(numPoints)

    for p in range(numPoints):

      outPoints.SetPoint(p, samplePoints[p,:])

    outData.SetPoints(outPoints)

    if a_OutFileName.endswith('vtp'):
      outWriter = vtkXMLPolyDataWriter()
    elif a_OutFileName.endswith('vtk'):
      outWriter = vtkPolyDataWriter()
    
    outWriter.SetInput(outData)
    outWriter.SetFileName(a_OutFileName)
    outWriter.Write()

  elif a_OutFileName.endswith('dat'):

    outWriter = open(a_OutFileName, 'w')
    outWriter.write(str(numPoints))
    for p in range(numPoints):
      outWriter.write("{0:10} {1:10} {2:10} \n".format(str(samplePoints[p,0]), str(samplePoints[p,1]), str(samplePoints[p,2])))

    outWriter.close()

  else:

    print("Unrecognized File Extension For Output Of Point Data")
    sys.exit()
    
  return samplePoints

#-------------------------------------------------------------------------------------
# THE FOLLOWING FUNCTIONS HAVE TO DO WITH EDITING AND SETTING UP THE FLOWVC INPUT FILE
#-------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------
## @brief Convert the entries in a FlowVC input file into a dictionary object for 
# rapid modification of input files
#
# @param[in] a_FVCInput         Name of the input file from which the entries are to be read
# @param[out] inputDictionary   A dictionary object (with values as strings) with all input fields
#
#-------------------------------------------------------------------------------------------------
def processFlowVCInputFileToDictionary(a_FVCInput):

    from collections import OrderedDict

    fileObj     = open(a_FVCInput, 'r')

    inputDictionary = OrderedDict()

    for line in fileObj:

        if (not(line.startswith('#')) and not(line.startswith(' '))):
            if line:
                lineSplit = line.split()
                if len(lineSplit) > 1:
                    inputDictionary[lineSplit[0]] = lineSplit[2]
    
    return inputDictionary

#--------------------------------------------------------------------------
## @brief Write a modified FlowVC data dictionary to a file
#
# @param[in] a_FVCDictionary    Dictionary object to be written into a file
# @param[in] a_FVCInput         Input file where the dictionary is written
#
#--------------------------------------------------------------------------
def writeDictionaryToFlowVCInput(a_FVCDictionary, a_FVCInput):

    from collections import OrderedDict

    fObj = open(a_FVCInput, 'w')

    for key, item in a_FVCDictionary.items():
        strWrite = str(key)+' = '+str(item)+'\n'
        fObj.write(strWrite)
        fObj.write('\n')

    fObj.close()

#---------------------------------------------------------------------------
## @brief Edit FlowVC input dictionary to change the input values for a simulation
#
# @param[in] a_FVCDictionary    Dictionary object for FlowVC input
# @param[in] a_FieldName        Input field that needs to be edited
# @param[in] a_FieldValue       Input field value that is to be set
#
#---------------------------------------------------------------------------
def editFlowVCInputDictionary(a_FVCInputDictionary, a_FieldName, a_FieldValue):
    
    from collections import OrderedDict

    a_FVCInputDictionary[a_FieldName] = a_FieldValue

#--------------------------------------------------------------------------------------
## @brief Display the values in the input file corresponding to a list of pre-specified 
# input fields, for the purpise of verifying whether correct input values have
# been included in the file
#
# @param[in] a_FVCDictionary    Dictionary object for FlowVC Input
# @param[in] a_FieldList        List of input fields to be verified
#
#-------------------------------------------------------------------------------------
def checkInputFields(a_FVCInputDictionary, a_FieldList, a_ValuesList):

    from collections import OrderedDict

    for key, item in a_FVCInputDictionary.items():

        if key in a_FieldList:
            keyIndex    = a_FieldList.index(key)
            keyVal      = a_ValuesList(keyIndex)

            if item != keyVal:
                print("Not Passed Checks: for", key)
                print("Please correct and re-run!")
                sys.exit()

#---------------------------------------------------------------------------------------
# THE FOLLOWING FUNCTIONS HAVE TO DO WITH EDITING AND SETTING UP THE FLOWVC PHYSICS FILE
#---------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------
## @brief Convert the entries in a FlowVC materials (.phys) file into a dictionary object for 
# rapid modification of input files
#
# @param[in] a_FVCPhys          Name of the .phys file from which the entries are to be read
# @param[out] inputDictionary   A dictionary object (with values as strings) with all input fields
#
#-------------------------------------------------------------------------------------------------
def processFlowVCPhysFileToDictionary(a_FVCPhys):

    from collections import OrderedDict
    
    fileObj     = open(a_FVCPhys, 'r')

    physDictionary = OrderedDict()

    for line in fileObj:

        if (not(line.startswith('#')) and not(line.startswith(' '))):
            if line:
                lineSplit = line.split()
                if len(lineSplit) > 1:
                    physDictionary[lineSplit[0]] = lineSplit[2]
    
    return physDictionary

#--------------------------------------------------------------------------
## @brief Write a modified FlowVC materials dictionary to a file
#
# @param[in] a_FVCDictionary    Dictionary object to be written into a file
# @param[in] a_FVCInput         Input file where the dictionary is written
#
#--------------------------------------------------------------------------
def writeDictionaryToFlowVCPhys(a_FVCDictionary, a_FVCInput):

    from collections import OrderedDict

    fObj = open(a_FVCInput, 'w')

    for key, item in a_FVCDictionary.items():
        strWrite = str(key)+' = '+str(item)+'\n'
        fObj.write(strWrite)
        fObj.write('\n')

    fObj.close()

#---------------------------------------------------------------------------------
## @brief Edit FlowVC input dictionary to change the input values for a simulation
#
# @param[in] a_FVCDictionary    Dictionary object for FlowVC phys file
# @param[in] a_FieldName        Input field that needs to be edited
# @param[in] a_FieldValue       Input field value that is to be set
#
#---------------------------------------------------------------------------------
def editFlowVCPhysDictionary(a_FVCPhysDictionary, a_FieldName, a_FieldValue):

    from collections import OrderedDict

    a_FVCPhysDictionary[a_FieldName] = a_FieldValue

#--------------------------------------------------------------------------------------
## @brief Display the values in the input file corresponding to a list of pre-specified 
# input fields, for the purpise of verifying whether correct input values have
# been included in the file
#
# @param[in] a_FVCPhysDictionary    Dictionary object for FlowVC Input
# @param[in] a_FieldList        List of input fields to be verified
#
#--------------------------------------------------------------------------------------
def checkPhysFields(a_FVCPhysDictionary, a_FieldList, a_ValuesList):

    from collections import OrderedDict

    for key, item in a_FVCPhysDictionary.items():

        if key in a_FieldList:
            keyIndex    = a_FieldList.index(key)
            keyVal      = a_ValuesList(keyIndex)

            if item != keyVal:
                print("Not Passed Checks: for", key)
                print("Please correct and re-run!")
                sys.exit()

#-----------------------------------------------------------------------------
## @brief Simple function with no input or return arguments to print the usage
# and the command line syntax for running the code as a script
#
#-----------------------------------------------------------------------------
def printScriptUsage():

  print("*****************************************************************")
  print(" A Python Utility Script For Creating Files To Setup Particle    ")
  print(" Simulations using FlowVC                                        ")
  print("                                                                 ")
  print(" Author: Debanjan Mukherjee, University of California, Berkeley  ")
  print("                                                                 ")
  print(" Usage Instructions:                                             ")
  print("--------------------                                             ")
  print(" To run the script type python preProcessFlowVC.py <mode>        ")
  print(" where <mode> is one of the following:                           ")
  print(" <mode> = particle-generate: generates the particle initial      ")
  print("          configuration file from a user specified VTK file      ")
  print(" <mode> = meshfiles-create: generates the coordinates, adjacency ")
  print("          and connectivity informations from data file           ")
  print(" <mode> = datafiles-create: generates the data files in binary   ")
  print("          format form input data files                           ")
  print(" <mode> = normals-create: generates the normals for the walls in ")
  print("          binary format from VTU file                            ")
  print(" <mode> = bflags-create: generates the boundary flags information")
  print(" <mode> = bbox-create: generates the bounding box for the mesh   ")
  print("*****************************************************************")

if __name__=='__main__':

    try: 
        import os
    except ImportError: 
        print("Could Not Load Module os")
  
    numArgs = len(sys.argv)

    if numArgs == 1:
    
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")

        printScriptUsage()
        sys.exit()

    elif numArgs >= 3:

        print("Invalid Number of Arguments at Command Line")
        sys.exit()

    else:
    
        executeMode = str(sys.argv[1])

    if executeMode == 'particle-generate':
     
        inputDir        = input('Enter Input Directory \n')
        inputFile       = input('Enter Meshfile Used To Create Particle Initi-config \n')
        vtkIn           = inputDir + inputFile
        outputDir       = input('Enter Output Directory \n')
        outputFile      = input('Enter Name Of Particle Init-config Output File \n')
        vtkOut          = outputDir + outputFile
        particleRadius  = input('Enter Particle Radius Value (+/- based input normals) \n')

        particleInit    = generateParticleInitialConfigFromMesh(vtkIn, vtkOut, a_TranslateRadius = -float(particleRadius), a_InputLegacyDataType = 'vtp')

    elif executeMode == 'meshfiles-create':

        inputDir    = input('Enter Input Directory \n')
        inputFile   = input('Enter Input File (vtk/vtu) \n')
        vtkIn       = inputDir + inputFile
        outputDir   = input('Enter Output Directory \n')
        outPrefix   = input('Enter Output Filename Prefix (see documentation) \n')
        outPrefix   = outputDir + outPrefix
        isAdj       = input('Enter YES To Create Binary Files For Adjacency \n')
         
        if isAdj == 'YES':
            createCoordinatesConnectivity(vtkIn, outPrefix, 'vtu', a_CreateAdjacency = True, a_Offset = 0)
        else:
            createCoordinatesConnectivity(vtkIn, outPrefix, 'vtu', a_CreateAdjacency = False, a_Offset = 0)

    elif executeMode == 'datafiles-create':

        dataDir         = input('Enter Directory Name Containing Data Files \n')
        numDataFiles    = input('Enter The Number Of Data Files (Time-steps) \n')
        dataPrefix      = input('Enter The Data File Prefix For The Input Files \n')
        dataExtension   = input('Enter The Extension For The Data Files (with the dot .) \n')
        outputDir       = input('Enter The Directory Name Where Outputs Are Dumped \n')
        outPrefix       = input('Enter The File Prefix Name For The Output Files \n')
        outPrefix       = outputDir + outPrefix
        startIndex      = input('Enter The Time Index Where Conversion Starts \n')
        endIndex        = input('Enter The Time Index Where Conversion Stops \n')
        stampInterval   = input('Enter The Time Interval Between Consecutive Time Indexed Files \n')
        indexInterval   = input('Enter The Difference Between Consecutive Time Indices \n')

        timeIndex = int(startIndex)
        timeStamp = 0.0
        
        for f in range(int(numDataFiles)):
            fileName    = dataDir+dataPrefix+str(timeIndex)+dataExtension
            fieldName   = 'velocity_'+'%1.5f'%(timeStamp)
            print(fieldName)
            convertVTKDataToBin(fileName, outPrefix, timeIndex, timeStamp, a_DataFieldName = fieldName, a_LegacyDataType = 'vtu')
            timeIndex = timeIndex + int(indexInterval)
            timeStamp = timeStamp + float(stampInterval)
    
    elif executeMode == 'normals-create':

        inputDir    = input('Enter Directory Containing Input Data Files \n')
        inputVTK    = input('Enter File Name (with extension) For Input Data \n')
        outputDir   = input('Enter Directory Name Where Outputs Are Dumped \n')
        outPrefix   = input('Enter The File Prefix Name For The Output Files \n')
        outPrefix   = outputDir + outPrefix
        dataField   = input('Enter Data Field Name (for velocity) \n')
        reverseN    = input('Enter YES To Reverse Normals (default is NO) \n')
        
        if reverseN == 'YES':
            convertSurfaceNormalsToBin(inputDir+inputVTK, outPrefix, a_VariableName = dataField, a_ReverseNormals = True, a_LegacyDataType = 'vtu')
        else:
            convertSurfaceNormalsToBin(inputDir+inputVTK, outPrefix, a_VariableName = dataField, a_ReverseNormals = False, a_LegacyDataType = 'vtu')

    elif executeMode == 'bflags-create':

        inputDir    = input('Enter Directory Containing Binary Data Files \n')
        binPrefix   = input('Enter Binary File Prefix as Prefix_vel.TIndex.bin \n')
        numDim      = input('Enter Geometry Dimension (3/2/1) \n')
        timeIndex   = input('Enter The Time Index To Be Used For Conversion \n')
        changePath  = input('Enter YES To Change The Path To The bdflags Executable \n')
        if changePath == 'YES':
            bdflagPath = input('Enter The Full Path To The bdflags Executable \n')
            createBoundaryFlagBinFile(inputDir+binPrefix, int(timeIndex), int(numDim), a_BDFlagsPath=bdflagPath)
        else:
            createBoundaryFlagBinFile(inputDir+binPrefix, int(timeIndex), int(numDim))

    elif executeMode == 'bbox-create':

        inputDir    = input('Enter Directory Containing The Mesh Data \n')
        fileName    = input('Enter Name Of The VTU Mesh File To Create Bounding Box \n')
        padding     = input('Enter A Constant Buffer Value \n')

        bbox        = generateMeshBounds(inputDir+fileName,float(padding))
        print("Bounding Box Coordinates")
        print("X0: {0:10} X1: {1:10}".format(str(bbox[0]), str(bbox[1])))
        print("Y0: {0:10} Y1: {1:10}".format(str(bbox[2]), str(bbox[3])))
        print("Z0: {0:10} Z1: {1:10}".format(str(bbox[4]), str(bbox[5])))

    else:

        sys.exit('Unrecognized Command Line Entry For The Script!')

