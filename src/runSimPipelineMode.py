#---------------------------------------------------------------------------------
## @file runSimPipelineMode.py
#  @author Debanjan Mukherjee, University of Colorado Boulder
#
# @brief This is a master script that runs the pre-processing steps for running 
# a tracer particle integration simulation using FlowVC developed by S.C. Shadden
# 
# @note Usage: To run this file simply type on a console the following command
# python3 runSimPipelineMode.py <configurationfile>
# where the <configurationfile> is a file with a .config extension
#
# @note Currently this only supports tracer integration, and FTLE computation. 
# Inertial particles, residence time, etc. are not included in the parsing 
# capabilities developed here.
#
#--------------------------------------------------------------------------------

#---------------------
# BEGIN MODULE IMPORTS
#---------------------
import sys, os

try:
    import configPrePost as CP
except ImportError:
    sys.exit("Could not import module configPrePost. Check configuration.")

try:
    import preProcessorFlowVC as PRE
except ImportError:
    sys.exit("Could not import module preProcessorFlowVC. Check configuration.")

try:
    import postProcessorFlowVC as POST
except ImportError:
    sys.exit("Could not import module postProcessorFlowVC. Check configuration.")
    
try:
    import subprocess
except ImportError:
    sys.exit("Could not import module subprocess. Check Python installation.")
#-------------------
# END MODULE IMPORTS
#-------------------

numArgs = len(sys.argv)

#----------------------------------
# read the input configuration file
#----------------------------------
if numArgs == 1:
    
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    print("This VCPrePost script needs to be executed as follows:")
    print("python3 runSimPipelineMode.py configuration.config")
    print("configuration.config is the input config data file")
    sys.exit()

elif numArgs == 2:
    
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    configFile  = str(sys.argv[1])

else:
    
    sys.exit("Invalid Number of Arguments at Command Line")

#-------------------------------------------------
# create the preprocessor confguration data object
#-------------------------------------------------
preConfig   = CP.configDataPreProcessor(configFile)
preConfig.setConfigData()
preConfig.printConfigData()

#---------------------------------------------------
# create the postprocessor configuration data object
#---------------------------------------------------
postConfig = CP.configDataPostProcessor(configFile)
postConfig.setConfigData()
postConfig.printConfigData()

#-------------------------------------------------
# create the FlowVC specific data container object
#-------------------------------------------------
fvConfig   = CP.configDataFlowVC(configFile)
fvConfig.setConfigData()
fvConfig.printConfigData()

#-------------------------------------------------------------
# for sim-pipeline mode it is mandatory to create the
# coordinates, connectivity, and adjacency binary files
#-------------------------------------------------------------
id0 = preConfig.getDataFileIndexStart()
id1 = preConfig.getDataFileIndexEnd()
Nd  = preConfig.getNumberOfDataFiles()
Did = preConfig.getDataFileIndexInterval()
DT  = preConfig.getDataFileTimeInterval()


dataIn      = preConfig.getDataFileName(id0)
outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix()

PRE.createCoordinatesConnectivity(dataIn, outPrefix,\
        a_InputLegacyDataType= preConfig.getDataFileType(), \
        a_CreateAdjacency = True, \
        a_Offset = 0)

#---------------------------------------------------------------------------
# for sim-pipeline mode it is mandatory to create velocity data binary files
#---------------------------------------------------------------------------
outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix() 
timeIndex   = id0
timeStamp   = 0.0
    
for f in range(Nd):
    fileName    = preConfig.getDataFileName(timeIndex)
    
    PRE.convertVTKDataToBin(fileName, outPrefix, timeIndex, timeStamp, \
            a_DataFieldName = preConfig.getDataFieldName(), \
            a_LegacyDataType = preConfig.getDataFileExtension())

    timeIndex   = timeIndex + Did
    timeStamp   = timeStamp + DT

#----------------------------------------------------
# for sim-pipeline mode it is mandatory to create the 
# mesh surface normal binary files
#----------------------------------------------------
dataIn      = preConfig.getDataFileName(id0)
outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix()
fieldName   = preConfig.getDataFieldName()

PRE.convertSurfaceNormalsToBin(dataIn, outPrefix, \
        a_VariableName = fieldName, \
        a_ReverseNormals = preConfig.flipNormalsAtConversion(), \
        a_LegacyDataType = preConfig.getDataFileType())

#--------------------------------------------
# for sim-pipeline mode it is mandatory to 
# obtain the global bounding box for the mesh
#--------------------------------------------
fileName    = preConfig.getDataFileName(id0)
padding     = 0.0
bbox        = PRE.generateMeshBounds(fileName, padding)
        
print("Bounding Box Coordinates")
print("X0: {0:10} X1: {1:10}".format(str(bbox[0]), str(bbox[1])))
print("Y0: {0:10} Y1: {1:10}".format(str(bbox[2]), str(bbox[3])))
print("Z0: {0:10} Z1: {1:10}".format(str(bbox[4]), str(bbox[5])))

#-----------------------------------------------------
# create the mesh boundary element marker binary files
#-----------------------------------------------------
if preConfig.convertBoundaryFlags() == True:

    appPath = os.path.dirname(os.path.realpath(__file__))
    
    if not appPath.endswith('/'):
        appPath = appPath + '/'

    outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix()
    numDim      = preConfig.getNumDimensions()
    bdflagPath  = appPath
    chooseId    = id0
    PRE.createBoundaryFlagBinFile(outPrefix, chooseId, numDim, \
            a_BDFlagsPath=bdflagPath)

#--------------------------------------------------------------------
# create tracer particle sample at seed locations specified by a file
# here, seed particles are manually created in preprocessing step or,
# they can be released as a cartesian grid with specified resolution
#--------------------------------------------------------------------
if preConfig.createSeedParticles() == True:

    seedMesh        = preConfig.getSeedMeshFileName()
    particleFile    = preConfig.getSeedParticleFileName() 
    particleRadius  = preConfig.getSeedParticleRadius()
    
    if preConfig.flipNormalsForParticles() == True:
        particleInit = PRE.generateParticleInitialConfigFromMesh(seedMesh, particleFile, \
                a_TranslateRadius = -particleRadius, \
                a_InputLegacyDataType = 'vtp')
    else:
        particleInit = PRE.generateParticleInitialConfigFromMesh(seedMesh, particleFile, \
                a_TranslateRadius = particleRadius, \
                a_InputLegacyDataType = 'vtp')
   

    if preConfig.subsampleParticles() == True:

        sampFreq = preConfig.getSeedSampleFreq()
        PRE.modifyParticleInitConfig(particleFile, particleFile, \
                a_EveryNPoints = sampFreq)

#-------------------------------------------------------------------------------------
# now we use the configDataFlowVC object, and the FlowVC data parser dictionary 
# programmed in preProcessorVC library to auto-generate a FlowVC compatible input file
# this step looks more like a very careful book-keeping and data configuration exercise
# but it is absolutely necessary to coordinate input data and settings across the 
# multiple different pieces of code.
#-------------------------------------------------------------------------------------
appPath = os.path.dirname(os.path.realpath(__file__))
    
if not appPath.endswith('/'):
    appPath = appPath + '/'

autoTemplateInputName   = appPath+"Input-Format.input"
projectInputName        = fvConfig.getInputFileName() 

vcInDict = PRE.processFlowVCInputFileToDictionary(autoTemplateInputName)

vcInDict['Path_Data']           = preConfig.getBinFileDirectory()
vcInDict['Path_Output']         = postConfig.getParticleDataDirectory()
vcInDict['Dimensions']          = preConfig.getNumDimensions()

vcInDict['Data_MeshType']       = str(1)    # default values

vcInDict['Data_InFilePrefix']   = preConfig.getBinFilePrefix()
vcInDict['Data_SuffixTMin']     = preConfig.getDataFileIndexStart()
vcInDict['Data_SuffixTDelta']   = preConfig.getDataFileIndexInterval()
vcInDict['Data_TRes']           = preConfig.getNumberOfDataFiles()
vcInDict['Data_TDelta']         = preConfig.getDataFileTimeInterval()

vcInDict['Data_TMin']           = str(0.0)  # default values

if preConfig.getDataPeriodicity() == 'T':
    vcInDict['Data_TPeriodic']      = 1
    vcInDict['Data_XPeriodic']      = 0
elif preConfig.getDataPeriodicity() == 'TX':
    vcInDict['Data_TPeriodic']      = 1
    vcInDict['Data_XPeriodic']      = 1
elif preConfig.getDataPeriodicity() == 'TV':
    vcInDict['Data_TPeriodic']      = 1
    vcInDict['Data_XPeriodic']      = 2
elif preConfig.getDataPeriodicity() == 'X':
    vcInDict['Data_TPeriodic']      = 0
    vcInDict['Data_XPeriodic']      = 1
elif preConfig.getDataPeriodicity() == 'V':
    vcInDict['Data_TPeriodic']      = 0
    vcInDict['Data_XPeriodic']      = 1
else:
    vcInDict['Data_TPeriodic']      = 0
    vcInDict['Data_XPeriodic']      = 0

vcInDict['Data_MeshBounds.XMin']    = str(bbox[0])
vcInDict['Data_MeshBounds.XMax']    = str(bbox[1])
vcInDict['Data_MeshBounds.YMin']    = str(bbox[2])
vcInDict['Data_MeshBounds.YMax']    = str(bbox[3])

if preConfig.getNumDimensions() == 2:
    vcInDict['Data_MeshBounds.ZMin']    = str(0.0)
    vcInDict['Data_MeshBounds.ZMax']    = str(0.0)
else:
    vcInDict['Data_MeshBounds.ZMin']    = str(bbox[4])
    vcInDict['Data_MeshBounds.ZMax']    = str(bbox[5])

if fvConfig.computeFiniteSize() == True:
    vcInDict['Fluid_Density']   = str(fvConfig.getFluidDensity())
    vcInDict['Fluid_Viscosity'] = str(fvConfig.getFluidViscosity())

vcInDict['Output_TStart']   = str(fvConfig.getOutputTStart()) 
vcInDict['Output_TRes']     = str(fvConfig.getOutputTRes())
vcInDict['Output_TDelta']   = str(fvConfig.getOutputTDelta())

vcInDict['Int_Type']                = str(fvConfig.getIntType())
vcInDict['Int_TimeStep']            = str(fvConfig.getIntTimeStep())
vcInDict['Int_Accuracy']            = str(fvConfig.getIntAccuracy())
vcInDict['Int_MinTimeStep']         = str(fvConfig.getIntMinSize())
vcInDict['Int_MaxTimeStep']         = str(fvConfig.getIntMaxSize())
vcInDict['Int_TimeDirection']       = str(fvConfig.getIntDirection())
vcInDict['Int_NormalFlow']          = str(fvConfig.getIntNormalFlow())
vcInDict['Int_NormalFlowScaling']   = str(fvConfig.getIntNormalScaling())
vcInDict['Int_Extrapolate']         = str(fvConfig.getIntExtrapolate())

vcInDict['Particle_Radius']         = str(preConfig.getSeedParticleRadius())
vcInDict['Particle_MaterialsFile']  = 'none'    # default values
vcInDict['Particle_ICType']         = str(1)    # default values

vcInDict['Gravity_Vector[0]'] = str(fvConfig.getGravityX())
vcInDict['Gravity_Vector[1]'] = str(fvConfig.getGravityY())
vcInDict['Gravity_Vector[2]'] = str(fvConfig.getGravityZ())

vcInDict['Physics_Model_Choice']    = str(0)    # default values
vcInDict['LocalSearchChecking']     = str(0)    # default values

if fvConfig.computeFTLE() == True:

    vcInDict['FTLE_Compute']            = 1
    
    gridBounds  = preConfig.getSeedGridBounds()

    vcInDict['FTLE_GenerateMesh']       = str(fvConfig.getFTLEGenerateMesh())
    vcInDict['FTLE_ICFile']             = fvConfig.getFTLEInitFile()
    vcInDict['FTLE_MeshBounds.XMin']    = str(gridBounds[0])
    vcInDict['FTLE_MeshBounds.XMax']    = str(gridBounds[1])
    vcInDict['FTLE_MeshBounds.YMin']    = str(gridBounds[2])
    vcInDict['FTLE_MeshBounds.YMax']    = str(gridBounds[3])
    vcInDict['FTLE_MeshBounds.ZMin']    = str(gridBounds[4])
    vcInDict['FTLE_MeshBounds.ZMax']    = str(gridBounds[5])
    vcInDict['FTLE_MeshBounds.XRes']    = str(preConfig.getSeedGridXResolution())
    vcInDict['FTLE_MeshBounds.YRes']    = str(preConfig.getSeedGridYResolution())
    vcInDict['FTLE_MeshBounds.ZRes']    = str(preConfig.getSeedGridZResolution())
    vcInDict['FTLE_IntTLength']         = str(fvConfig.getFTLEIntLength())

    vcInDict['FTLE_ComputeVariation']   = str(0)    # default value
    vcInDict['FTLE_VariationOutFreq']   = str(0)    # default value
    
    vcInDict['FTLE_OutFilePrefix']      = postConfig.getParticleFilePrefix() 
    
elif fvConfig.computeFTLE() == False:
    
    vcInDict['FTLE_Compute'] = 0

if fvConfig.computeTracers() == True:
    
    vcInDict['Trace_Compute'] = 1
    
    if preConfig.createSeedCartesian() == True:
        vcInDict['Trace_GenerateMesh'] = str(1)     # default value
    else:
        vcInDict['Trace_GenerateMesh']  = str(0)    # default value
        vcInDict['Trace_InFileFormat']  = str(2)    # default value
        
        vcInDict['Trace_InFile']        = preConfig.getSeedParticleFileName().split('/')[-1]

    vcInDict['Trace_AlwaysOutput']      = str(1)    # default value
    vcInDict['Trace_MultipleInFiles']   = str(0)    # default value

    vcInDict['Trace_OutFilePrefix']     = postConfig.getParticleFilePrefix()
    vcInDict['Trace_ReleaseStrategy']   = str(fvConfig.getTracerReleaseType())
    vcInDict['Trace_ReleaseTMax']       = str(fvConfig.getTracerReleaseDuration())
    vcInDict['Trace_NumLaunchTimes']    = str(fvConfig.getNumTracerLaunch())
    vcInDict['Trace_LaunchTimeSpacing'] = str(fvConfig.getTracerLaunchSpacing())
    vcInDict['Trace_IntTLength']        = str(fvConfig.getMaxTracerIntTime())
    
    gridBounds  = preConfig.getSeedGridBounds()
    
    vcInDict['Trace_MeshBounds.XMin']   = str(gridBounds[0])
    vcInDict['Trace_MeshBounds.XMax']   = str(gridBounds[1])
    vcInDict['Trace_MeshBounds.YMin']   = str(gridBounds[2])
    vcInDict['Trace_MeshBounds.YMax']   = str(gridBounds[3])
    vcInDict['Trace_MeshBounds.ZMin']   = str(gridBounds[4])
    vcInDict['Trace_MeshBounds.ZMax']   = str(gridBounds[5])
    vcInDict['Trace_MeshBounds.XRes']   = str(preConfig.getSeedGridXResolution())
    vcInDict['Trace_MeshBounds.YRes']   = str(preConfig.getSeedGridYResolution())
    vcInDict['Trace_MeshBounds.ZRes']   = str(preConfig.getSeedGridZResolution())

elif fvConfig.computeTracers() == False:
    
    vcInDict['Trace_Compute'] = 0

PRE.writeDictionaryToFlowVCInput(vcInDict, projectInputName)

#---------------------------------------------------------------------
# then we use the generated project input to run the FlowVC simulation
# further tweaks to the subprocess module can also be programmed to 
# make this automated execution more systematic
#---------------------------------------------------------------------
appPath = os.path.dirname(os.path.realpath(__file__))
    
if not appPath.endswith('/'):
    appPath = appPath + '/'

pCommList = [appPath+'flowVC', projectInputName]

subprocess.call(pCommList)

#----------------------------------------------------------------------
# then we use the postprocessing configuration to convert particle data
# into output vtk files for analysis and visualization
#----------------------------------------------------------------------
appPath = os.path.dirname(os.path.realpath(__file__))
    
if not appPath.endswith('/'):
    appPath = appPath + '/'

dataType = postConfig.getParticleDataType()

if fvConfig.computeTracers()==True:

    filePrefix = postConfig.getParticleDataDirectory() + postConfig.getParticleFilePrefix()
    POST.scriptBinToVtkParticles(filePrefix, postConfig.getParticleIndexStart(), postConfig.getParticleIndexStop(), \
            a_ND = preConfig.getNumDimensions(),\
            a_Bin2VtkPath = appPath)

elif fvConfig.computeFTLE()==True:

    filePrefix = postConfig.getParticleDataDirectory() + postConfig.getParticleFilePrefix()
    POST.scriptBinToVtkFTLE(filePrefix, postConfig.getParticleIndexStart(), postConfig.getParticleIndexStop(), \
            a_ND = preConfig.getNumDimensions(),\
            a_Bin2VtkPath = appPath)



