################################# flowVC-2.0 PHYSICS FILE ###################################
#
# NOTE: Order of parameters listed must be maintained for proper parsing of input file
#	Comment lines begin with #, comments can be added or removed as desired
#	Standard format: VARIABLE_NAME = VALUE
#
#############################################################################################
#
# Variable: Particle_Density, Type: Double
# Particle density (in consistent units)
#
Particle_Density = 0.0
#
# Variable: Contact_Model_Choice, Type: Integer
# The choice variable to choose between different implementations of contact models
# The current allowable model choices are
# 0 - Fixed restitution coefficient given by Int_NormalFlowScaling
# 1 - Elasto-plastic, velocity dependent, contact
# 2 - Viscoelastic contact
# 3 - Viscoelastic, velocity dependent, contact
#
Contact_Model_Choice = 0
#
# Variable: Particle_Elasticity, Type: Double
# Elasticity modulus of the particle (in consistent units)
#
Particle_Elasticity = 0.0
#
# Variable: Particle_Poisson, Type: Double
# Poisson ratio for the particle material (dimensionless)
#
Particle_Poisson = 0.0
#
# Variable: Particle_Bulk_Visco, Type: Double
# Equivalent bulk modulus for dissipative component of stress tensor 
# for the particle material (in consistent units)
#
Particle_Bulk_Visco = 0.0
#
# Variable: Particle_Dev_Visco, Type: Double
# Equivalent shear modulus for dissipative component of stress tensor
# for the particle material (in consistent units)
#
Particle_Dev_Visco = 0.0
#
# Variable: Particle_YieldStress, Type: Double
# Elastoplastic Yield stress for the particle material (in consistent units)
#
Particle_YieldStress = 0.0
#
# Variable: Wall_Elasticity, Type: Double
# Elasticity modulus for the wall material (in consistent units)
#
Wall_Elasticity = 0.0
#
# Variable: Wall_Poisson, Type: Double
# Poisson ratio for the wall material (dimensionless)
#
Wall_Poisson = 0.0
#
# Variable: Wall_Bulk_Visco, Type: Double
# Equivalent bulk modulus for the dissipative component of the stress tensor
# for the wall material (in consistent units)
#
Wall_Bulk_Visco = 0.0
#
# Variable: Wall_Dev_Visco, Type: Double
# Equivalent shear modulus for the dissipative component of the stress tensor
# for the wall material (in consistent units)
#
Wall_Dev_Visco = 0.0
#
# Variable: Wall_YieldStress, Type: Double
# Elastoplastic Yield stress for the wall material (in consistent units)
#
Wall_YieldStress = 0.0
#
# Variable: Monitor_Forces, Type: Integer
# Choice variable for setting a force monitor
# value = 1 -> monitor is switched on
# value = 0 -> monitor is switched off
#
Monitor_Forces = 0
#
# Variable: Force_MonitorFile, Type: String
# The filename (without the .monvc extension) for recording the monitor values
# The actual file gets saved at "Path_Data/Force_MonitorFile.monvc"
#
Force_MonitorFile = forceMonitor
#
# Variable: Monitor_Type, Type: String
# The parameters, or quantities to be monitored during the simulation
# The following entries will be allowed:
# 1 - all force magnitudes only
# 2 - all force vectors only
# 3 - all force powers only
# 4 - particle velocity histories
#
Monitor_Type = 1
#
# Variable: Stokesian_Indicator, Type:Integer
# Indicator variable to turn on(1)/off(0) the Stokes drag assumption
#
Stokesian_Indicator = 0
#
# Variable: Lift_Indicator, Type: Integer
# Indicator variable for turning shear-gradient driven lift forces on(1)/off(0)
#
Lift_Indicator = 1
#
# Variable: Lubrication_Indicator, Type: Integer
# Indicator variable for turning wall-lubrication forces on(1)/off(0)
#
Lubrication_Indicator = 1
#
# Variable: Brownian_Indicator, Type: Integer
# Indicator variable for turning brownian forces on(1)/off(0)
#
Brownian_Indicator = 0
#
# Variable: Faxen_Indicator, Type: Integer
# Indicator variable for turning Faxen correction terms on(1)/off(0)
#
Faxen_Indicator = 0
#
# Variable: NearWall_Indicator, Type: Integer
# Indicator variable for turning on advanced models for near wall 
# drag and lift corrections on(1)/off(0)
NearWall_Indicator = 0
