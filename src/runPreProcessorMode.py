#---------------------------------------------------------------------------------
## @file runPreProcessorMode.py
#  @author Debanjan Mukherjee, University of Colorado Boulder
#
# @brief This is a master script that runs the pre-processing steps for running 
# a tracer particle integration simulation using FlowVC developed by S.C. Shadden
# 
# @note Usage: To run this file simply type on a console the following command
# python3 runPreProcessorMode.py <configurationfile>
# where the <configurationfile> is a file with a .config extension
#
#--------------------------------------------------------------------------------

#---------------------
# BEGIN MODULE IMPORTS
#---------------------
import sys, os

try:
    import configPrePost as CP
except ImportError:
    sys.exit("Could not import module configPrePost. Check configuration.")

try:
    import preProcessorFlowVC as PRE
except ImportError:
    sys.exit("Could not import module preProcessorFlowVC. Check configuration.")
#-------------------
# END MODULE IMPORTS
#-------------------

numArgs = len(sys.argv)

#----------------------------------
# read the input configuration file
#----------------------------------
if numArgs == 1:
    
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    print("This VCPrePost script needs to be executed as follows:")
    print("python3 runSimPipelineMode.py configuration.config")
    print("configuration.config is the input config data file")
    sys.exit()

elif numArgs == 2:
    
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    configFile  = str(sys.argv[1])

else:
    
    sys.exit("Invalid Number of Arguments at Command Line")

#-------------------------------------------------
# create the preprocessor confguration data object
#-------------------------------------------------
print("Reading Configuration File")
preConfig   = CP.configDataPreProcessor(configFile)
preConfig.setConfigData()
preConfig.printConfigData()

id0 = preConfig.getDataFileIndexStart()
id1 = preConfig.getDataFileIndexEnd()
Nd  = preConfig.getNumberOfDataFiles()
Did = preConfig.getDataFileIndexInterval()
DT  = preConfig.getDataFileTimeInterval()

#-------------------------------------------------------------
# create coordinates, connectivity, and adjacency binary files
#-------------------------------------------------------------
if preConfig.convertMeshFiles() == True:
    
    print("Converting Mesh Files: Coordinates, Connectivity, Adjacency")
    dataIn      = preConfig.getDataFileName(id0)
    outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix() 
    
    PRE.createCoordinatesConnectivity(dataIn, outPrefix, \
            a_InputLegacyDataType = preConfig.getDataFileType(), \
            a_CreateAdjacency = True, \
            a_Offset = 0)

#----------------------------------
# create velocity data binary files
#----------------------------------
if preConfig.convertDataFiles() == True:
    
    print("Converting Flow Data Files")
    outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix() 
    timeIndex   = id0
    timeStamp   = 0.0
    
    for f in range(Nd):
        fileName    = preConfig.getDataFileName(timeIndex)

        PRE.convertVTKDataToBin(fileName, outPrefix, timeIndex, timeStamp, \
                a_DataFieldName = preConfig.getDataFieldName(),\
                a_LegacyDataType = preConfig.getDataFileType())

        timeIndex   = timeIndex + Did
        timeStamp   = timeStamp + DT

#--------------------------------------------
# create the mesh surface normal binary files
#--------------------------------------------
if preConfig.convertNormals() == True:
    
    print("Converting Surface Normals")
    dataIn      = preConfig.getDataFileName(id0)
    outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix()
    fieldName   = preConfig.getDataFieldName()

    PRE.convertSurfaceNormalsToBin(dataIn, outPrefix, \
            a_VariableName = fieldName, \
            a_ReverseNormals = preConfig.flipNormalsAtConversion(), \
            a_LegacyDataType = preConfig.getDataFileType())

#-----------------------------------------------------
# create the mesh boundary element marker binary files
#-----------------------------------------------------
if preConfig.convertBoundaryFlags() == True:
    
    print("Converting Boundary Cell IDs")
    appPath = os.path.dirname(os.path.realpath(__file__))
    
    if not appPath.endswith('/'):
        appPath = appPath + '/'

    outPrefix   = preConfig.getBinFileDirectory() + preConfig.getBinFilePrefix()
    numDim      = preConfig.getNumDimensions()
    bdflagPath  = appPath
    chooseId    = id0
    PRE.createBoundaryFlagBinFile(outPrefix, chooseId, numDim, \
            a_BDFlagsPath=bdflagPath)

#--------------------------------------------
# obtain the global bounding box for the mesh
#--------------------------------------------
if preConfig.obtainBoundingBox() == True:

        fileName    = preConfig.getDataFileName(id0)
        padding     = 0.0
        bbox        = PRE.generateMeshBounds(fileName, padding)
        
        print("Bounding Box Coordinates")
        print("X0: {0:10} X1: {1:10}".format(str(bbox[0]), str(bbox[1])))
        print("Y0: {0:10} Y1: {1:10}".format(str(bbox[2]), str(bbox[3])))
        print("Z0: {0:10} Z1: {1:10}".format(str(bbox[4]), str(bbox[5])))

#--------------------------------------------------------------------
# create tracer particle sample at seed locations specified by a file
#--------------------------------------------------------------------
if preConfig.createSeedParticles() == True:

    seedMesh        = preConfig.getSeedMeshFileName()
    particleFile    = preConfig.getSeedParticleFileName() 
    particleRadius  = preConfig.getSeedParticleRadius()
    
    if preConfig.flipNormalsForParticles() == True:
        particleInit = PRE.generateParticleInitialConfigFromMesh(seedMesh, particleFile, \
                a_TranslateRadius = -particleRadius, \
                a_InputLegacyDataType = 'vtp')
    else:
        particleInit = PRE.generateParticleInitialConfigFromMesh(seedMesh, particleFile, \
                a_TranslateRadius = particleRadius, \
                a_InputLegacyDataType = 'vtp')
   

    if preConfig.subsampleParticles() == True:

        sampFreq = preConfig.getSeedSampleFreq()
        PRE.modifyParticleInitConfig(particleFile, particleFile, \
                a_EveryNPoints = sampFreq)




