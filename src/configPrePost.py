#--------------------------------------------------------------------------------
## @file File containing data objects that read a configuration input file and 
# generate a configuration of pre and post processing operations. 
#
# @author Debanjan Mukherjee, University of Colorado Boulder
#
# @brief Definition of two classes that encapsulate data required to configure 
# a set of preprocessing and postprocessing operations.
#
# @note To test the encapsulation and verify data/configuration settings:
# type: python3 configPrePost.py
#
#--------------------------------------------------------------------------------

#---------------------
# BEGIN MODULE IMPORTS
#---------------------
from __future__ import print_function
import sys, os
#---------------------
# END MODULE IMPORTS
#---------------------

#---------------------------------------------------------------------------------------------------
## @brief Class that containerizes all input variables required to run pre-processing 
# operations, to be parsed in/out through a *.config file.
#
# @var m_ConfigFile     The name of the *.config file to parse data from
# @var m_NumDim         Number of spatial dimensions (integer)
# @var m_DataDir        Directory containing vtk flow data files (string)
# @var m_NumDataFiles   Number of vtk data files in the directory (integer)
# @var m_DataPrefix     Prefix string in data file name format:prefixID.vtk (string)
# @var m_DataExtension  Filename extension for data files (vtu/vtp/vtk) (string)
# @var m_BinDir         Directory where *.bin files for FlowVC are to be dumped (string)
# @var m_BinPrefix      Prefix string in bin file name format: prefix.ID.bin (string)
# @var m_LegacyType     The type pf mesh data file (vtu/vtp/vts/vti) etc. (string)
# @var m_StartIndex     Starting index in file name index series (ID) (integer)
# @var m_StopIndex      Final index in file name index series (ID) (integer)
# @var m_Timeinterval   Interval of actual/wall time between each file (float)
# @var m_IndexInterval  Interval between indices (ID's) for each file (integer)
# @var m_DataFieldName  Field name for velocity data (check in Paraview) (string)
# @var m_SeedPDir       Directry containing mesh file for seeding particles (string)
# @var m_SeedMesh       File name of the mesh file for seeding particles (string)
# @var m_SeedDir        Directory where file containing seeded particles will be placed (string)
# @var m_SeedFile       Name of the file containing seed particles (string)
# @var m_ParticleRadius Radius of seed particles (for non-tracer data) (float)
# @var m_SeedSubsampleFreq Frequency of subsampling (1 for every N samples) seed particles (integer)
# @var m_GridBounds     Bounding box of cartesian grid for particle seedig (float X 6)
# @var m_GridNumX       Number of seed points on cartesian grid along X coordinate (int)
# @var m_GridNumY       Number of seed points on cartesian grid along Y coordinate (int)
# @var m_GridNumZ       Number of seed points on cartesian grid along Z coordinate (int)
# @var m_IsCreateMeshFiles  Boolean flag for creating coordinates, connectivity bin files (True/False)
# @var m_IsCreateDataFiles  Boolean flag for creating converted velocity/data bin files (True/False)
# @var m_IsCreateNormal     Boolean flag for creating converted surface normal bin files (True/False)
# @var m_IsFlipNormals      Boolean flag for flipping normals (to make outward normals +ve) (True/False)
# @var m_IsCreateBFlags     Boolean flag for creating boundary cell ID bin files (True/False)
# @var m_IsCreateBBox       Boolean flag for computing and outputing boundary box (True/False)
# @var m_IsCreateParticles  Boolean flag for creating sequence of seed particles (True/False)
# @var m_IsSeedFlipNormals  Boolean flag for flipping the surface normals for particle seeding (True/False)
# @var m_IsSeedSubSample    Boolean flag to sub-sample the number of seed points (True/False)
# @var m_IsSeedView         Boolean flag to visualize the created seed particles (True/False)
# @var m_IsSeedGrid         Boolean flag to create seed particles on a cartesian grid (True/False)
#
#-------------------------------------------------------------------------------------------------------
class configDataPreProcessor:

    #---------------------------------------------------------------
    ## @brief Class constructor
    #
    # @param[in] a_ConfigFile Name of config file to parse data from
    #
    #---------------------------------------------------------------
    def __init__(self, a_ConfigFile):
        self.m_ConfigFile = a_ConfigFile
    
    #----------------------------------------------------------------------
    ## @brief Function to parse input config file and set config parameters
    #
    #----------------------------------------------------------------------
    def setConfigData(self):

        fObj    = open(self.m_ConfigFile, 'r')
        
        for line in fObj:

            if not line.startswith('//') and line.startswith('PRE:'):

                lineDict = line.split('=')
                
                if lineDict[0].strip() == 'PRE: Geometry Dimensions':

                    self.m_NumDim = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Data File Directory':

                    self.m_DataDir = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Data File Count':

                    self.m_NumDataFiles = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Data File Prefix':

                    self.m_DataPrefix = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Data File Extension':

                    self.m_DataExtension = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Data Periodicity':

                    self.m_DataPeriodic = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Bin File Directory':

                    self.m_BinDir = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Bin File Prefix':

                    self.m_BinPrefix = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Data Conversion - File Type':

                    self.m_LegacyType = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Data Conversion - Index Start':

                    self.m_StartIndex = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Index End':

                    self.m_StopIndex = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Time Interval':

                    self.m_TimeInterval = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Index Interval':

                    self.m_IndexInterval = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Convert Meshfiles':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsCreateMeshFiles = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsCreateMeshFiles = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())
                        
                elif lineDict[0].strip() == 'PRE: Data Conversion - Convert Datafiles':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsCreateDataFiles = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsCreateDataFiles = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Data Fieldname':

                    self.m_DataFieldName = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Data Conversion - Convert Normals':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsCreateNormals = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsCreateNormals = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Flip Normals':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsFlipNormals = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsFlipNormals = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PRE: Data Conversion - Convert Boundaryflags':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsCreateBFlags = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsCreateBFlags = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())
                        
                elif lineDict[0].strip() == 'PRE: Data Conversion - Bounding Box':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsCreateBBox = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsCreateBBox = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())
                        
                elif lineDict[0].strip() == 'PRE: Initialize Particles':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsCreateParticles = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsCreateParticles = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())
                        
                elif lineDict[0].strip() == 'PRE: Initialize Particles - Input Directory':

                    if lineDict[1].strip() == 'default':
                        self.m_SeedPDir = self.m_DataDir
                    else:
                        self.m_SeedPDir = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Initialize Particles - Seed Meshfile':

                    self.m_SeedMesh = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Initialize Particles - Output Directory':

                    if lineDict[1].strip() == 'default':
                        self.m_SeedDir = self.m_BinDir
                    else:
                        self.m_SeedDir = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Initialize Particles - Particle File':

                    self.m_SeedFile = lineDict[1].strip()

                elif lineDict[0].strip() == 'PRE: Initialize Particles - Particle Radius':

                    self.m_ParticleRadius = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Particles - Flip Normals':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsSeedFlipNormals = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsSeedFlipNormals = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Particles - Seed Subsample':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsSeedSubsample = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsSeedSubsample = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())
                        
                elif lineDict[0].strip() == 'PRE: Initialize Particles - Subsample Frequency':

                    self.m_SeedSubsampleFreq = int(lineDict[1].strip())
                    
                elif lineDict[0].strip() == 'PRE: Initialize Particles - View Particles':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsSeedView = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsSeedView = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Particles On Grid':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsSeedGrid = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsSeedGrid = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Mesh Bounds Xmin':

                    self.m_GridBounds = [0.0 for n in range(6)]
                    self.m_GridBounds[0] = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Mesh Bounds Xmax':

                    self.m_GridBounds[1] = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Mesh Bounds Ymin':

                    self.m_GridBounds[2] = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Mesh Bounds Ymax':

                    self.m_GridBounds[3] = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Mesh Bounds Zmin':
                    
                    self.m_GridBounds[4] = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Mesh Bounds Zmax':

                    self.m_GridBounds[0] = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Grid Resolution X':

                    self.m_GridNumX = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Grid Resolution Y':

                    self.m_GridNumY = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: Initialize Grid - Grid Resolution Z':

                    self.m_GridNumZ = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PRE: End Preprocessor':
                    
                    break
    
    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the number of spatial dimensions
    #
    #-----------------------------------------------------------------------------
    def getNumDimensions(self):
        return self.m_NumDim

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the directory containing flow data
    #
    #-----------------------------------------------------------------------------
    def getDataDir(self): 
        return self.m_DataDir

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the space or time periodicity 
    #
    #-----------------------------------------------------------------------------
    def getDataPeriodicity(self):
        return self.m_DataPeriodic

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the number of data files
    #
    #-----------------------------------------------------------------------------
    def getNumberOfDataFiles(self):
        return self.m_NumDataFiles

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the prefix in flow data file name
    #
    #-----------------------------------------------------------------------------
    def getDataFilePrefix(self):
        return self.m_DataPrefix

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the file extension for flow data files
    #
    #-----------------------------------------------------------------------------
    def getDataFileExtension(self):
        return self.m_DataExtension

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the file type for flow data files
    #
    #-----------------------------------------------------------------------------
    def getDataFileType(self):
        return self.m_LegacyType

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the starting index in data file series
    #
    #-----------------------------------------------------------------------------
    def getDataFileIndexStart(self):
        return self.m_StartIndex

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the final index in data file series
    #
    #-----------------------------------------------------------------------------
    def getDataFileIndexEnd(self):
        return self.m_StopIndex

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the complete name for specified data ID
    #
    #-----------------------------------------------------------------------------
    def getDataFileName(self, a_Index):
        return self.m_DataDir + self.m_DataPrefix + str(a_Index) + self.m_DataExtension

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns time interval between data file IDs
    #
    #-----------------------------------------------------------------------------
    def getDataFileTimeInterval(self):
        return self.m_TimeInterval

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns index interval between data file IDs
    #
    #-----------------------------------------------------------------------------
    def getDataFileIndexInterval(self):
        return self.m_IndexInterval

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns directory where .bin files are created
    #
    #-----------------------------------------------------------------------------
    def getBinFileDirectory(self):
        return self.m_BinDir

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the prefix in .bin file name
    #
    #-----------------------------------------------------------------------------
    def getBinFilePrefix(self):
        return self.m_BinPrefix

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the field name for data variable
    #
    #-----------------------------------------------------------------------------
    def getDataFieldName(self):
        return self.m_DataFieldName

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns name of meshfile for seeding particles
    #
    #-----------------------------------------------------------------------------
    def getSeedMeshFileName(self):
        return self.m_SeedPDir + self.m_SeedMesh

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns name of particle seed file
    #
    #-----------------------------------------------------------------------------
    def getSeedParticleFileName(self):
        return self.m_SeedDir + self.m_SeedFile

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the radius of finite size particles
    #
    #-----------------------------------------------------------------------------
    def getSeedParticleRadius(self):
        return self.m_ParticleRadius

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the subsampling frequency for seeding
    #
    #-----------------------------------------------------------------------------
    def getSeedSampleFrequency(self):
        return self.m_SeedSubsampleFreq
    
    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for coordinate/connectivity conversion
    #
    #-----------------------------------------------------------------------------
    def convertMeshFiles(self):
        return self.m_IsCreateMeshFiles
    
    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for flow data conversion
    #
    #-----------------------------------------------------------------------------
    def convertDataFiles(self):
        return self.m_IsCreateDataFiles

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for surface normal conversion
    #
    #-----------------------------------------------------------------------------
    def convertNormals(self):
        return self.m_IsCreateNormals

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for flipping outward normals
    #
    #-----------------------------------------------------------------------------
    def flipNormalsAtConversion(self):
        return self.m_IsFlipNormals

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for boundary cell id conversion
    #
    #-----------------------------------------------------------------------------
    def convertBoundaryFlags(self):
        return self.m_IsCreateBFlags

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for bounding box computation
    #
    #-----------------------------------------------------------------------------
    def obtainBoundingBox(self):
        return self.m_IsCreateBBox

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for creating seed particles
    #
    #-----------------------------------------------------------------------------
    def createSeedParticles(self):
        return self.m_IsCreateParticles

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for seeding particles on a grid
    #
    #-----------------------------------------------------------------------------
    def createSeedCartesian(self):
        return self.m_IsSeedGrid

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for flipping normals for seeding
    #
    #-----------------------------------------------------------------------------
    def flipNormalsForParticles(self):
        return self.m_IsSeedFlipNormals

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for subsampled particle seeding
    #
    #-----------------------------------------------------------------------------
    def subsampleParticles(self):
        return self.m_IsSeedSubsample

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns bounding box for particle cartesian grid
    #
    #-----------------------------------------------------------------------------
    def getSeedGridBounds(self):
        return self.m_GridBounds

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns number of seed grid points along X
    #
    #-----------------------------------------------------------------------------
    def getSeedGridXResolution(self):
        return self.m_GridNumX

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns number of seed grid points along Y
    #
    #-----------------------------------------------------------------------------
    def getSeedGridYResolution(self):
        return self.m_GridNumY

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns number of seed grid points along Z
    #
    #-----------------------------------------------------------------------------
    def getSeedGridZResolution(self):
        return self.m_GridNumZ

    #----------------------------------------------------------
    ## @brief Display the current set of parameter variables 
    #
    #----------------------------------------------------------
    def printConfigData(self):

        print("--------------------------------------")
        print("Postprocessor Configured As Below    :")
        print("--------------------------------------")
        print("Number Of Geometry Dimensions        :", self.m_NumDim)
        print("Directory Containing Data Files      :", self.m_DataDir)
        print("Number Of Data Files                 :", self.m_NumDataFiles)
        print("Prefix In Data File Naming-scheme    :", self.m_DataPrefix)
        print("File Extension For Data Files        :", self.m_DataExtension)
        print("Directory To Create Binary Files     :", self.m_BinDir)
        print("Prefix In Binary File Naming-scheme  :", self.m_BinPrefix)
        print("--------------------------------------")
        print("Data Conversion Configured As Below  :")
        print("--------------------------------------")
        print("Datafile Series - File Type          :", self.m_LegacyType)
        print("Datafile Series Name - Starting Index:", self.m_StartIndex)
        print("Datafile Series Name - Stopping Index:", self.m_StopIndex)
        print("Interval Between Datafile Indices    :", self.m_IndexInterval)
        print("Interval Between Datafile Timing     :", self.m_TimeInterval)
        print("Convert Mesh And Adjacency Data?     :", self.m_IsCreateMeshFiles)
        print("Convert Velocity Data Time Series?   :", self.m_IsCreateDataFiles)
        print("Data Field Name In Data Time Series  :", self.m_DataFieldName)
        print("Convert Surface Normal Vectors?      :", self.m_IsCreateNormals)
        print("Flip Surface Normals At Conversion?  :", self.m_IsFlipNormals)
        print("Obtain Domain Boundary Flags?        :", self.m_IsCreateBBox)
        print("Convert Boundary Cell ID Data?       :", self.m_IsCreateBFlags)
        print("--------------------------------------")
        print("Particle Seeding Configured As Below :")
        print("--------------------------------------")
        print("Seed Particles In Preprocessing Step?:", self.m_IsCreateParticles)
        print("Directory Containing Seed Mesh File  :", self.m_SeedPDir)
        print("Name Of The Particle Seed Mesh File  :", self.m_SeedFile)
        print("Directory To Dump Seeded Particles   :", self.m_SeedDir)
        print("Name Of Particle Seed File           :", self.m_SeedFile)
        print("Radius Of Finite Size Particles      :", self.m_ParticleRadius)
        print("Flip Surface Normals For Seeding?    :", self.m_IsSeedFlipNormals)
        print("Subsample The Seed Points?           :", self.m_IsSeedSubsample)
        print("Subsample 1 For Every N Points, N=   :", self.m_SeedSubsampleFreq)
        print("Visualize Particle Seeds?            :", self.m_IsSeedView)
        print("Initialize Particles On XYZ Grid?    :", self.m_IsSeedGrid)
        print("X Coordinate Bounds For XYZ Grid     :", self.m_GridBounds[0], self.m_GridBounds[1])
        print("Y Coordinate Bounds For XYZ Grid     :", self.m_GridBounds[2], self.m_GridBounds[3])
        print("Z Coordinate Bounds For XYZ Grid     :", self.m_GridBounds[4], self.m_GridBounds[5])
        print("Number Of Grid Points Along X        :", self.m_GridNumX)
        print("Number Of Grid Points Along Y        :", self.m_GridNumY)
        print("Number Of Grid Points Along Z        :", self.m_GridNumZ)
        

#---------------------------------------------------------------------------------------------------
## @brief Class that containerizes all input variables required to run post-processing  
# operations, to be parsed in/out through a *.config file.
#
# @var m_ConfigFile         The name of the *.config file to parse data from
# @var m_TracerDataType     Type of particle based data generated from simulation (string)
# @var m_TracerDirectory    Name of output directory that contains resultant particle data (string)
# @var m_TracerPrefix       Prefix in filename system: Prefix.ID.bin (string)
# @var m_TracerStartIndex   Starting index in particle file time series (integer)
# @var m_TracerStopIndex    Final index in particle file time series (integer)
# @var m_TracerDeltaIndex   Interval between successive indices in time series (integer)
#
#-------------------------------------------------------------------------------------------------------
class configDataPostProcessor:

    #---------------------------------------------------------------
    ## @brief Class constructor
    #
    # @param[in] a_ConfigFile Name of config file to parse data from
    #
    #---------------------------------------------------------------
    def __init__(self, a_ConfigFile):
        self.m_ConfigFile = a_ConfigFile

    #----------------------------------------------------------------------
    ## @brief Function to parse input config file and set config parameters
    #
    #----------------------------------------------------------------------
    def setConfigData(self):

        fObj    = open(self.m_ConfigFile, 'r')

        for line in fObj:

            if not line.startswith('//') and line.startswith('PST:'):

                lineDict = line.split('=')
                
                if lineDict[0].strip() == 'PST: Convert Particle Data':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsConvertTracers = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsConvertTracers = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'PST: Particle Conversion - Data Type':

                    self.m_TracerDataType = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PST: Particle Conversion - Data Directory':

                    self.m_TracerDirectory = lineDict[1].strip()

                elif lineDict[0].strip() == 'PST: Particle Conversion - File Prefix':

                    self.m_TracerPrefix = lineDict[1].strip()

                elif lineDict[0].strip() == 'PST: Particle Conversion - Index Start':

                    self.m_TracerStartIndex = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PST: Particle Conversion - Index End':

                    self.m_TracerStopIndex  = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PST: Particle Conversion - Index Interval':

                    self.m_TracerDeltaIndex = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'PST: End Postprocessor':
                    
                    break

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns flag for starting particle conversion
    #
    #-----------------------------------------------------------------------------
    def convertParticles(self):
        return self.m_IsConvertTracers

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the particle data type
    #
    #-----------------------------------------------------------------------------
    def getParticleDataType(self):
        return self.m_TracerDataType

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the particle data directory name
    #
    #-----------------------------------------------------------------------------
    def getParticleDataDirectory(self):
        return self.m_TracerDirectory

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the prefix for particle filename
    #
    #-----------------------------------------------------------------------------
    def getParticleFilePrefix(self):
        return self.m_TracerPrefix

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns starting index in particle time-series
    #
    #-----------------------------------------------------------------------------
    def getParticleIndexStart(self):
        return self.m_TracerStartIndex

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns final index in particle time-series
    #
    #-----------------------------------------------------------------------------
    def getParticleIndexStop(self):
        return self.m_TracerStopIndex

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns interval between successive time indices
    #
    #-----------------------------------------------------------------------------
    def getParticleIndexInterval(self):
        return self.m_TracerDeltaIndex

    #----------------------------------------------------------
    ## @brief Display the current set of parameter variables 
    #
    #----------------------------------------------------------
    def printConfigData(self):

        print("--------------------------------------")
        print("Postprocessor Configured As Below    :")
        print("--------------------------------------")
        print("Convert Particle Data Files?         :", self.m_IsConvertTracers)
        print("Particle Data Directory Name         :", self.m_TracerDirectory)
        print("Particle Data File Prefix            :", self.m_TracerPrefix)
        print("Particle Data File Index - Start     :", self.m_TracerStartIndex)
        print("Particle Data File Index - End       :", self.m_TracerStopIndex)
        print("Particle Data File Index Interval    :", self.m_TracerDeltaIndex)

#-------------------------------------------------------------------------------------------------------
## @brief Class that containerizes all additional input variables that are required to generate a 
# parsed version of FlowVC simulation inputs that can be executed from within the VCPrePost package
#
# @todo Complete implementation for parser, complete class and methods documentation: configDataFlowVC
#-------------------------------------------------------------------------------------------------------
class configDataFlowVC:

    #---------------------------------------------------------------
    ## @brief Class constructor
    #
    # @param[in] a_ConfigFile Name of config file to parse data from
    #
    #---------------------------------------------------------------
    def __init__(self, a_ConfigFile):
        self.m_ConfigFile = a_ConfigFile

    #----------------------------------------------------------------------
    ## @brief Function to parse input config file and set config parameters
    #
    #----------------------------------------------------------------------
    def setConfigData(self):

        fObj    = open(self.m_ConfigFile, 'r')

        for line in fObj:

            if not line.startswith('//') and line.startswith('FVC:'):

                lineDict = line.split('=')
                
                if lineDict[0].strip() == 'FVC: Project Input Filename':

                    self.m_InputFile = lineDict[1].strip()

                elif lineDict[0].strip() == 'FVC: Fluid Properties - Viscosity':

                    self.m_Viscosity = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Fluid Properties - Density':

                    self.m_Density = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Fluid Properties - Gravity X':

                    self.m_GravityX = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Fluid Properties - Gravity Y':

                    self.m_GravityY = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Fluid Properties - Gravity Z':

                    self.m_GravityZ = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Output Timing - Simulation Start':

                    self.m_TStart   = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Output Timing - Number Of Timepoints':

                    self.m_TRes   = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Output Timing - Time Interval':

                    self.m_TDelta   = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Integration Type':

                    self.m_IntType  = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Time Step':

                    self.m_IntTimeStep = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Integration Accuracy':

                    self.m_IntAccuracy = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Minimum Stepsize':

                    self.m_IntMinSize = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Maximum Stepsize':

                    self.m_IntMaxSize = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Time Direction':

                    self.m_IntDirection = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Normal Correction':

                    self.m_IntNormalFlow = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Normal Scaling':

                    self.m_IntNormalScaling = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Time Integration - Compute Extrapolation':
                    
                    if lineDict[1].strip() == 'TRUE':
                        self.m_IntExtrapolate = 1
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IntExtrapolate = 0
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'FVC: Compute FTLE':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsComputeFTLE = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsComputeFTLE = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())      

                elif lineDict[0].strip() == 'FVC: FTLE Computation - Generate Meshfile':

                    self.m_FTLEGenerateMesh = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: FTLE Computation - Initialization File':

                    self.m_FTLEInitFile = lineDict[1].strip()

                elif lineDict[0].strip() == 'FVC: FTLE Computation - Integration Duration':

                    self.m_FTLEIntLength = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: FTLE Computation - Output Prefix':

                    self.m_FTLEPrefix = lineDict[1].strip()

                elif lineDict[0].strip() == 'FVC: Compute Tracers':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsComputeTracers = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsComputeTracers = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'FVC: Tracer Computation - Tracer Release Strategy':

                    self.m_TraceRelease = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Tracer Computation - Staggered Release Duration':

                    self.m_TraceReleaseTMax = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Tracer Computation - Release Instant Count':

                    self.m_NumLaunchTimes = int(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Tracer Computation - Release Instant Intervals':

                    self.m_LaunchTimeSpacing = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Tracer Computation - Maximum Integration Duration':

                    self.m_TraceIntTMax = float(lineDict[1].strip())

                elif lineDict[0].strip() == 'FVC: Compute Finite Size':

                    if lineDict[1].strip() == 'TRUE':
                        self.m_IsComputeFinite = True
                    elif lineDict[1].strip() == 'FALSE':
                        self.m_IsComputeFinite = False
                    else:
                        sys.exit("Incorrect user input: "+lineDict[0].strip())

                elif lineDict[0].strip() == 'FVC: End FlowVC':
                    
                    break

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns binary flag for computing FTLE
    #
    #-----------------------------------------------------------------------------    
    def computeFTLE(self):
        return self.m_IsComputeFTLE

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns binary flag for computing tracer paths
    #
    #-----------------------------------------------------------------------------    
    def computeTracers(self):
        return self.m_IsComputeTracers

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns binary flag for inertial particle paths
    #
    #-----------------------------------------------------------------------------    
    def computeFiniteSize(self):
        return self.m_IsComputeFinite

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the .input filename for FlowVC to read
    #
    #-----------------------------------------------------------------------------    
    def getInputFileName(self):
        return self.m_InputFile

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the fluid viscosity value
    #
    #-----------------------------------------------------------------------------    
    def getFluidViscosity(self):
        return self.m_Viscosity

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the fluid density value
    #
    #-----------------------------------------------------------------------------    
    def getFluidDensity(self):
        return self.m_Density

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns global x component of gravity
    #
    #-----------------------------------------------------------------------------    
    def getGravityX(self):
        return self.m_GravityX

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns global y component of gravity
    #
    #-----------------------------------------------------------------------------
    def getGravityY(self):
        return self.m_GravityY

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns global z component of gravity
    #
    #----------------------------------------------------------------------------- 
    def getGravityZ(self):
        return self.m_GravityZ

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns initial time to start simulation and write output     
    #
    #----------------------------------------------------------------------------- 
    def getOutputTStart(self):
        return self.m_TStart

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the desired number of output times
    #
    #----------------------------------------------------------------------------- 
    def getOutputTRes(self):
        return self.m_TRes

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the desired time between successive outputs
    #
    #----------------------------------------------------------------------------- 
    def getOutputTDelta(self):
        return self.m_TDelta

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the type of integration scheme for paricles
    #
    #----------------------------------------------------------------------------- 
    def getIntType(self): 
        return self.m_IntType

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the time step used for integration
    #
    #----------------------------------------------------------------------------- 
    def getIntTimeStep(self):
        return self.m_IntTimeStep

    #----------------------------------------------------------------------------------
    ## @brief Accessor function that returns the desired numerical integration accuracy
    #
    #---------------------------------------------------------------------------------- 
    def getIntAccuracy(self):
        return self.m_IntAccuracy

    #-----------------------------------------------------------------------------------
    ## @brief Accessor function that returns the min. step-size for adaptive integration
    #
    #----------------------------------------------------------------------------------- 
    def getIntMinSize(self):
        return self.m_IntMinSize

    #-----------------------------------------------------------------------------------
    ## @brief Accessor function that returns the max. step-size for adaptive integration
    #
    #----------------------------------------------------------------------------------- 
    def getIntMaxSize(self):
        return self.m_IntMaxSize

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns 1/-1 for fwd/bwd integration in time
    #
    #----------------------------------------------------------------------------- 
    def getIntDirection(self):
        return self.m_IntDirection

    #-----------------------------------------------------------------------------------
    ## @brief Accessor function that returns the binary flag for wall boundary condition
    #
    #----------------------------------------------------------------------------------- 
    def getIntNormalFlow(self):
        return self.m_IntNormalFlow

    #-----------------------------------------------------------------------------------------------
    ## @brief Accessor function that returns the scaling parameter used for wall boundary conditions
    #
    #----------------------------------------------------------------------------------------------- 
    def getIntNormalScaling(self):
        return self.m_IntNormalScaling

    #------------------------------------------------------------------------------------------
    ## @brief Accessor function that returns the flag to integrate particles outside the domain
    #
    #------------------------------------------------------------------------------------------
    def getIntExtrapolate(self):
        return self.m_IntExtrapolate

    #------------------------------------------------------------------------------------------
    ## @brief Accessor function that returns binary flag for generating cartesian grid for FTLE
    #
    #------------------------------------------------------------------------------------------ 
    def getFTLEGenerateMesh(self):
        return self.m_FTLEGenerateMesh

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns name of intialization file for FTLE 
    #
    #----------------------------------------------------------------------------- 
    def getFTLEInitFile(self):
        return self.m_FTLEInitFile

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the integration duration for FTLE
    #
    #----------------------------------------------------------------------------- 
    def getFTLEIntLength(self):
        return self.m_FTLEIntLength

    #-----------------------------------------------------------------------------
    ## @brief Accessor function that returns the filename prefix for FTLE output
    #
    #----------------------------------------------------------------------------- 
    def getFTLEOutPrefix(self):
        return self.m_FTLEPrefix

    #---------------------------------------------------------------------------------
    ## @brief Accessor function that returns tracer particle release - fixed/staggered
    #
    #--------------------------------------------------------------------------------- 
    def getTracerReleaseType(self):
        return self.m_TraceRelease

    #----------------------------------------------------------------------------------
    ## @brief Accessor function that returns the release duration for staggered release
    #
    #---------------------------------------------------------------------------------- 
    def getTracerReleaseDuration(self):
        return self.m_TraceReleaseTMax

    #-----------------------------------------------------------------------------------
    ## @brief Accessor function that returns the number of fixed tracer release instants
    #
    #----------------------------------------------------------------------------------- 
    def getNumTracerLaunch(self):
        return self.m_NumLaunchTimes

    #-------------------------------------------------------------------------------------
    ## @brief Accessor function that returns the interval between succesive fixed launches
    #
    #------------------------------------------------------------------------------------- 
    def getTracerLaunchSpacing(self):
        return self.m_LaunchTimeSpacing

    #------------------------------------------------------------------------------------
    ## @brief Accessor function that returns the maximum duration for integrating tracers
    #
    #------------------------------------------------------------------------------------ 
    def getMaxTracerIntTime(self):
        return self.m_TraceIntTMax
    
    #----------------------------------------------------------
    ## @brief Display the current set of parameter variables 
    #
    #----------------------------------------------------------
    def printConfigData(self):
        
        print("--------------------------------------")
        print("FlowVC Solver Configured As Below    :")
        print("--------------------------------------")
        print("FlowVC .input File Name              :", self.m_InputFile)
        print("Fluid Viscosity Value                :", self.m_Viscosity)
        print("Fluid Density Value                  :", self.m_Density)
        print("X Component Of Gravity               :", self.m_GravityX)
        print("Y Component Of Gravity               :", self.m_GravityY)
        print("Z Component Of Gravity               :", self.m_GravityZ)
        print("Simulation Output Start Time         :", self.m_TStart)
        print("Number Of Output Data Time Instances :", self.m_TRes)
        print("Interval Between Output Times        :", self.m_TDelta)
        print("--------------------------------------")
        print("Time Integration Configured As Below :")
        print("--------------------------------------")
        print("Numerical Integration Choice         :", self.m_IntType)
        print("Integration Time Step Size           :", self.m_IntTimeStep)
        print("Numerical Integration Accuracy       :", self.m_IntAccuracy)
        print("Maximum Step Size For Adaptive Scheme:", self.m_IntMaxSize)
        print("Minimum Step Size For Adaptive Scheme:", self.m_IntMinSize)
        print("Integration Time Direction (fwd/bwd) :", self.m_IntDirection)
        print("Normal Flow Correction At Walls      :", self.m_IntNormalFlow)
        print("Normal Flow Correction Parameter     :", self.m_IntNormalScaling)
        print("Extrapolation Outside Domains        :", self.m_IntExtrapolate)
        print("--------------------------------------")
        print("FTLE Computation Configured As Below :")
        print("--------------------------------------")
        print("Generate Cartesian Grid For FTLE     :", self.m_FTLEGenerateMesh)
        print("Name Of FTLE Initialization File     :", self.m_FTLEInitFile)
        print("Total Integration Duration For FTLE  :", self.m_FTLEIntLength)
        print("Prefix For FTLE Output File Name     :", self.m_FTLEPrefix)
        print("--------------------------------------")
        print("Tracer Analysis Configured As Below  :")
        print("--------------------------------------")

#--------------------------------------------------------------------
## @brief Built in testing for any line of parsing completed from the 
# configuration (*.config) files
#
#--------------------------------------------------------------------
if __name__ == "__main__":

    if len(sys.argv) != 2:
        
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print("In Test Mode You Have To Enter A File Name To Test With")
        print("Enter: python3 configPrePost.py <filename>")

    else:

        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")

        testConfigPre   = configDataPreProcessor(sys.argv[1].strip())
        testConfigPre.setConfigData()
        testConfigPre.printConfigData()

        testConfigPost  = configDataPostProcessor(sys.argv[1].strip())
        testConfigPost.setConfigData()
        testConfigPost.printConfigData()

        testConfigVC    = configDataFlowVC(sys.argv[1].strip())
        testConfigVC.setConfigData()
        testConfigVC.printConfigData()

