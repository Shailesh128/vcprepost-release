\contentsline {chapter}{\numberline {1}Documentation for the V\-C\-Pre\-Post config file}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Todo List}{7}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Class Index}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Class List}{9}{section.3.1}% 
\contentsline {chapter}{\numberline {4}File Index}{11}{chapter.4}% 
\contentsline {section}{\numberline {4.1}File List}{11}{section.4.1}% 
\contentsline {chapter}{\numberline {5}Class Documentation}{13}{chapter.5}% 
\contentsline {section}{\numberline {5.1}config\-Pre\-Post.\-config\-Data\-Flow\-V\-C Class Reference}{13}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{15}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Constructor \& Destructor Documentation}{15}{subsection.5.1.2}% 
\contentsline {subsubsection}{\numberline {5.1.2.1}\-\_\-\-\_\-init\-\_\-\-\_\-}{15}{subsubsection.5.1.2.1}% 
\contentsline {subsection}{\numberline {5.1.3}Member Function Documentation}{15}{subsection.5.1.3}% 
\contentsline {subsubsection}{\numberline {5.1.3.1}compute\-Finite\-Size}{15}{subsubsection.5.1.3.1}% 
\contentsline {subsubsection}{\numberline {5.1.3.2}compute\-F\-T\-L\-E}{15}{subsubsection.5.1.3.2}% 
\contentsline {subsubsection}{\numberline {5.1.3.3}compute\-Tracers}{16}{subsubsection.5.1.3.3}% 
\contentsline {subsubsection}{\numberline {5.1.3.4}get\-Fluid\-Density}{16}{subsubsection.5.1.3.4}% 
\contentsline {subsubsection}{\numberline {5.1.3.5}get\-Fluid\-Viscosity}{16}{subsubsection.5.1.3.5}% 
\contentsline {subsubsection}{\numberline {5.1.3.6}get\-F\-T\-L\-E\-Generate\-Mesh}{16}{subsubsection.5.1.3.6}% 
\contentsline {subsubsection}{\numberline {5.1.3.7}get\-F\-T\-L\-E\-Init\-File}{16}{subsubsection.5.1.3.7}% 
\contentsline {subsubsection}{\numberline {5.1.3.8}get\-F\-T\-L\-E\-Int\-Length}{16}{subsubsection.5.1.3.8}% 
\contentsline {subsubsection}{\numberline {5.1.3.9}get\-F\-T\-L\-E\-Out\-Prefix}{16}{subsubsection.5.1.3.9}% 
\contentsline {subsubsection}{\numberline {5.1.3.10}get\-Gravity\-X}{16}{subsubsection.5.1.3.10}% 
\contentsline {subsubsection}{\numberline {5.1.3.11}get\-Gravity\-Y}{16}{subsubsection.5.1.3.11}% 
\contentsline {subsubsection}{\numberline {5.1.3.12}get\-Gravity\-Z}{17}{subsubsection.5.1.3.12}% 
\contentsline {subsubsection}{\numberline {5.1.3.13}get\-Input\-File\-Name}{17}{subsubsection.5.1.3.13}% 
\contentsline {subsubsection}{\numberline {5.1.3.14}get\-Int\-Accuracy}{17}{subsubsection.5.1.3.14}% 
\contentsline {subsubsection}{\numberline {5.1.3.15}get\-Int\-Direction}{17}{subsubsection.5.1.3.15}% 
\contentsline {subsubsection}{\numberline {5.1.3.16}get\-Int\-Extrapolate}{17}{subsubsection.5.1.3.16}% 
\contentsline {subsubsection}{\numberline {5.1.3.17}get\-Int\-Max\-Size}{17}{subsubsection.5.1.3.17}% 
\contentsline {subsubsection}{\numberline {5.1.3.18}get\-Int\-Min\-Size}{17}{subsubsection.5.1.3.18}% 
\contentsline {subsubsection}{\numberline {5.1.3.19}get\-Int\-Normal\-Flow}{17}{subsubsection.5.1.3.19}% 
\contentsline {subsubsection}{\numberline {5.1.3.20}get\-Int\-Normal\-Scaling}{17}{subsubsection.5.1.3.20}% 
\contentsline {subsubsection}{\numberline {5.1.3.21}get\-Int\-Time\-Step}{18}{subsubsection.5.1.3.21}% 
\contentsline {subsubsection}{\numberline {5.1.3.22}get\-Int\-Type}{18}{subsubsection.5.1.3.22}% 
\contentsline {subsubsection}{\numberline {5.1.3.23}get\-Max\-Tracer\-Int\-Time}{18}{subsubsection.5.1.3.23}% 
\contentsline {subsubsection}{\numberline {5.1.3.24}get\-Num\-Tracer\-Launch}{18}{subsubsection.5.1.3.24}% 
\contentsline {subsubsection}{\numberline {5.1.3.25}get\-Output\-T\-Delta}{18}{subsubsection.5.1.3.25}% 
\contentsline {subsubsection}{\numberline {5.1.3.26}get\-Output\-T\-Res}{18}{subsubsection.5.1.3.26}% 
\contentsline {subsubsection}{\numberline {5.1.3.27}get\-Output\-T\-Start}{18}{subsubsection.5.1.3.27}% 
\contentsline {subsubsection}{\numberline {5.1.3.28}get\-Tracer\-Launch\-Spacing}{18}{subsubsection.5.1.3.28}% 
\contentsline {subsubsection}{\numberline {5.1.3.29}get\-Tracer\-Release\-Duration}{18}{subsubsection.5.1.3.29}% 
\contentsline {subsubsection}{\numberline {5.1.3.30}get\-Tracer\-Release\-Type}{19}{subsubsection.5.1.3.30}% 
\contentsline {subsubsection}{\numberline {5.1.3.31}print\-Config\-Data}{19}{subsubsection.5.1.3.31}% 
\contentsline {subsubsection}{\numberline {5.1.3.32}set\-Config\-Data}{19}{subsubsection.5.1.3.32}% 
\contentsline {section}{\numberline {5.2}config\-Pre\-Post.\-config\-Data\-Post\-Processor Class Reference}{19}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Constructor \& Destructor Documentation}{20}{subsection.5.2.1}% 
\contentsline {subsubsection}{\numberline {5.2.1.1}\-\_\-\-\_\-init\-\_\-\-\_\-}{20}{subsubsection.5.2.1.1}% 
\contentsline {subsection}{\numberline {5.2.2}Member Function Documentation}{20}{subsection.5.2.2}% 
\contentsline {subsubsection}{\numberline {5.2.2.1}convert\-Particles}{20}{subsubsection.5.2.2.1}% 
\contentsline {subsubsection}{\numberline {5.2.2.2}get\-Particle\-Data\-Directory}{20}{subsubsection.5.2.2.2}% 
\contentsline {subsubsection}{\numberline {5.2.2.3}get\-Particle\-Data\-Type}{20}{subsubsection.5.2.2.3}% 
\contentsline {subsubsection}{\numberline {5.2.2.4}get\-Particle\-File\-Prefix}{20}{subsubsection.5.2.2.4}% 
\contentsline {subsubsection}{\numberline {5.2.2.5}get\-Particle\-Index\-Interval}{20}{subsubsection.5.2.2.5}% 
\contentsline {subsubsection}{\numberline {5.2.2.6}get\-Particle\-Index\-Start}{20}{subsubsection.5.2.2.6}% 
\contentsline {subsubsection}{\numberline {5.2.2.7}get\-Particle\-Index\-Stop}{21}{subsubsection.5.2.2.7}% 
\contentsline {subsubsection}{\numberline {5.2.2.8}print\-Config\-Data}{21}{subsubsection.5.2.2.8}% 
\contentsline {subsubsection}{\numberline {5.2.2.9}set\-Config\-Data}{21}{subsubsection.5.2.2.9}% 
\contentsline {section}{\numberline {5.3}config\-Pre\-Post.\-config\-Data\-Pre\-Processor Class Reference}{21}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Constructor \& Destructor Documentation}{23}{subsection.5.3.1}% 
\contentsline {subsubsection}{\numberline {5.3.1.1}\-\_\-\-\_\-init\-\_\-\-\_\-}{23}{subsubsection.5.3.1.1}% 
\contentsline {subsection}{\numberline {5.3.2}Member Function Documentation}{23}{subsection.5.3.2}% 
\contentsline {subsubsection}{\numberline {5.3.2.1}convert\-Boundary\-Flags}{24}{subsubsection.5.3.2.1}% 
\contentsline {subsubsection}{\numberline {5.3.2.2}convert\-Data\-Files}{24}{subsubsection.5.3.2.2}% 
\contentsline {subsubsection}{\numberline {5.3.2.3}convert\-Mesh\-Files}{24}{subsubsection.5.3.2.3}% 
\contentsline {subsubsection}{\numberline {5.3.2.4}convert\-Normals}{24}{subsubsection.5.3.2.4}% 
\contentsline {subsubsection}{\numberline {5.3.2.5}create\-Seed\-Cartesian}{24}{subsubsection.5.3.2.5}% 
\contentsline {subsubsection}{\numberline {5.3.2.6}create\-Seed\-Particles}{24}{subsubsection.5.3.2.6}% 
\contentsline {subsubsection}{\numberline {5.3.2.7}flip\-Normals\-At\-Conversion}{24}{subsubsection.5.3.2.7}% 
\contentsline {subsubsection}{\numberline {5.3.2.8}flip\-Normals\-For\-Particles}{24}{subsubsection.5.3.2.8}% 
\contentsline {subsubsection}{\numberline {5.3.2.9}get\-Bin\-File\-Directory}{24}{subsubsection.5.3.2.9}% 
\contentsline {subsubsection}{\numberline {5.3.2.10}get\-Bin\-File\-Prefix}{25}{subsubsection.5.3.2.10}% 
\contentsline {subsubsection}{\numberline {5.3.2.11}get\-Data\-Dir}{25}{subsubsection.5.3.2.11}% 
\contentsline {subsubsection}{\numberline {5.3.2.12}get\-Data\-Field\-Name}{25}{subsubsection.5.3.2.12}% 
\contentsline {subsubsection}{\numberline {5.3.2.13}get\-Data\-File\-Extension}{25}{subsubsection.5.3.2.13}% 
\contentsline {subsubsection}{\numberline {5.3.2.14}get\-Data\-File\-Index\-End}{25}{subsubsection.5.3.2.14}% 
\contentsline {subsubsection}{\numberline {5.3.2.15}get\-Data\-File\-Index\-Interval}{25}{subsubsection.5.3.2.15}% 
\contentsline {subsubsection}{\numberline {5.3.2.16}get\-Data\-File\-Index\-Start}{25}{subsubsection.5.3.2.16}% 
\contentsline {subsubsection}{\numberline {5.3.2.17}get\-Data\-File\-Name}{25}{subsubsection.5.3.2.17}% 
\contentsline {subsubsection}{\numberline {5.3.2.18}get\-Data\-File\-Prefix}{25}{subsubsection.5.3.2.18}% 
\contentsline {subsubsection}{\numberline {5.3.2.19}get\-Data\-File\-Time\-Interval}{26}{subsubsection.5.3.2.19}% 
\contentsline {subsubsection}{\numberline {5.3.2.20}get\-Data\-File\-Type}{26}{subsubsection.5.3.2.20}% 
\contentsline {subsubsection}{\numberline {5.3.2.21}get\-Data\-Periodicity}{26}{subsubsection.5.3.2.21}% 
\contentsline {subsubsection}{\numberline {5.3.2.22}get\-Number\-Of\-Data\-Files}{26}{subsubsection.5.3.2.22}% 
\contentsline {subsubsection}{\numberline {5.3.2.23}get\-Num\-Dimensions}{26}{subsubsection.5.3.2.23}% 
\contentsline {subsubsection}{\numberline {5.3.2.24}get\-Seed\-Grid\-Bounds}{26}{subsubsection.5.3.2.24}% 
\contentsline {subsubsection}{\numberline {5.3.2.25}get\-Seed\-Grid\-X\-Resolution}{26}{subsubsection.5.3.2.25}% 
\contentsline {subsubsection}{\numberline {5.3.2.26}get\-Seed\-Grid\-Y\-Resolution}{26}{subsubsection.5.3.2.26}% 
\contentsline {subsubsection}{\numberline {5.3.2.27}get\-Seed\-Grid\-Z\-Resolution}{26}{subsubsection.5.3.2.27}% 
\contentsline {subsubsection}{\numberline {5.3.2.28}get\-Seed\-Mesh\-File\-Name}{27}{subsubsection.5.3.2.28}% 
\contentsline {subsubsection}{\numberline {5.3.2.29}get\-Seed\-Particle\-File\-Name}{27}{subsubsection.5.3.2.29}% 
\contentsline {subsubsection}{\numberline {5.3.2.30}get\-Seed\-Particle\-Radius}{27}{subsubsection.5.3.2.30}% 
\contentsline {subsubsection}{\numberline {5.3.2.31}get\-Seed\-Sample\-Frequency}{27}{subsubsection.5.3.2.31}% 
\contentsline {subsubsection}{\numberline {5.3.2.32}obtain\-Bounding\-Box}{27}{subsubsection.5.3.2.32}% 
\contentsline {subsubsection}{\numberline {5.3.2.33}print\-Config\-Data}{27}{subsubsection.5.3.2.33}% 
\contentsline {subsubsection}{\numberline {5.3.2.34}set\-Config\-Data}{27}{subsubsection.5.3.2.34}% 
\contentsline {subsubsection}{\numberline {5.3.2.35}subsample\-Particles}{27}{subsubsection.5.3.2.35}% 
\contentsline {chapter}{\numberline {6}File Documentation}{29}{chapter.6}% 
\contentsline {section}{\numberline {6.1}post\-Processor\-Flow\-V\-C.\-py File Reference}{29}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}Detailed Description}{29}{subsection.6.1.1}% 
\contentsline {section}{\numberline {6.2}pre\-Processor\-Flow\-V\-C.\-py File Reference}{29}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Detailed Description}{32}{subsection.6.2.1}% 
\contentsline {section}{\numberline {6.3}run\-Post\-Processor\-Mode.\-py File Reference}{32}{section.6.3}% 
\contentsline {subsection}{\numberline {6.3.1}Detailed Description}{33}{subsection.6.3.1}% 
\contentsline {section}{\numberline {6.4}run\-Pre\-Processor\-Mode.\-py File Reference}{33}{section.6.4}% 
\contentsline {subsection}{\numberline {6.4.1}Detailed Description}{34}{subsection.6.4.1}% 
\contentsline {section}{\numberline {6.5}run\-Sim\-Pipeline\-Mode.\-py File Reference}{34}{section.6.5}% 
\contentsline {subsection}{\numberline {6.5.1}Detailed Description}{35}{subsection.6.5.1}% 
